Heroes Tower Defense

All the infos about how to compile and launch the game are here in french : 
http://heroestowerdefense.scienceontheweb.net/compilation.html

If you want more infos about it :
http://heroestowerdefense.scienceontheweb.net/

All the musics are free of copyright, and come from : 
https://www.sounddesigners.org/ and https://lasonotheque.org/ and maybe some other websites.
