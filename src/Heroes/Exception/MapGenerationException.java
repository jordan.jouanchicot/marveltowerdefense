package Heroes.Exception;

//Classe d'exception en cas d'erreur de chargement de la map

/**
 * MapGenException
 * @author Jordan Rey-Jouanchicot
 */
public class MapGenerationException extends Exception{
    public MapGenerationException() {
        super("Could not generate a map correctly. Generating a new one.");
    }
}
