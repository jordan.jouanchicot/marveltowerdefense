package Heroes.Exception;

//Classe d'exception si un problème de ressource survient
/**
 * RessourceNotFoundException
 * @author Jordan Rey-Jouanchicot
 */
public class RessourceNotFoundException  extends Exception {
    public RessourceNotFoundException() {
            super("A ressource necessary for this project was not found. Please download the game again.");
    }
}
