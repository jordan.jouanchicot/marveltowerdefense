package Heroes.Exception;

//Classe d'exception si un problème survient pour accéder au jeu
/**
 * Stage Exception : case game stage issue
 * @author Jordan Rey-Jouanchicot
 */
public class StageException extends Exception {
    public StageException() {
        super("A problem occurred when trying to get access to the Game.");
    }
}