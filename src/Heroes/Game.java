package Heroes;

import Heroes.MenuInterface.Settings;
import Heroes.Music.Music;
import Heroes.Tile.Towers.Attacks.Attack;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.util.Duration;
import Heroes.Input.Mouse;
import Heroes.Map.MapOfTiles;
import Heroes.MenuInterface.Main;
import Heroes.Monster.Monster;

import Heroes.Monster.Wave;
import Heroes.Notification.Notification;
import Heroes.Tile.Tile;
import Heroes.Tile.TileKinds;
import Heroes.Tile.TowerInfos;
import Heroes.Tile.Towers.*;
import Heroes.User.InGameUser;
import Heroes.User.User;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import static java.lang.Thread.sleep;

/**
 * The type Game.
 */
public class Game extends Application{
    private static int width = 1200;
    private static int height = width / 16 * 9;
    private static Mouse mouse = new Mouse(1);

    private static TowerInfos towerselected;
    private static HashMap<Integer,Tower> lt = new HashMap<>();
    private static BorderPane pane = new BorderPane();
    private static BorderPane pane3;
    private static BorderPane pane4;
    private static HBox pane1;
    private static User user;
    private static InGameUser inGameUser;
    private static boolean paused = true;
    private static Music music;
    private static MapOfTiles map = new MapOfTiles();
    private static Pane root = new StackPane();

    private Wave wave;
    private static ArrayList<Attack> attackList= new ArrayList<Attack>();
    private double timer = 0;
    private Timeline gameLoop;
    private boolean running;

    /**
     * Instantiates a new Game.
     *
     * @param stage the stage
     * @throws IOException the io exception
     */
    public Game(Stage stage) throws IOException {
        // setup stage to not resizable size
        stage.setResizable(false);
        // initiate music
        music = new Music("ambiance.mp3");
        // initiate inGame user
        inGameUser = new InGameUser();
        // create and load user
        user = new User();
        user.loadUser();
        // load settingss
        Settings settings = new Settings();
        if (!settings.loadSettings()){
            settings.saveSettings();
        }
        // settup stage and panes
        stage.setTitle("Heroes Tower Defense");
        root.setPrefSize(Game.width+200,Game.height+50);
        pane.setId("test");
        pane1 = (HBox) FXMLLoader.load(Game.class.getResource("/Heroes/TopInGameMenu/TopInGameMenu.fxml"));
        pane.setCenter(root);
        Scene scene = new Scene(pane,Game.width+200,Game.height+30);
        scene.getStylesheets().add("Heroes/style.css");

        // create, update and render map
        map.createMap();
        map.updateMap();
        map.renderMap(root);

        // set mouse handling
        EventHandler<MouseEvent> mouseHandler = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                mouse.setX(mouseEvent.getX());
                mouse.setY(mouseEvent.getY());
                System.out.println(mouse.getTileKey());

                System.out.println(towerselected);
                try {
                    buyTower(map,root,stage);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };

        // initiate path list
        ArrayList<Integer> path = new ArrayList<>(map.getPathList());

        // generate first wave
        wave = new Wave(1, path, root);

        // declare gameloop
        EventHandler<ActionEvent> gameUpdate = event ->
        {
            // if not paused and running
            if (running&&(!paused)){
                // animate and move monster
                animateMove(root, path, stage);
                timer++;
                // for each tower created set attacking to false
                for (Tower t: lt.values()) {
                    t.setAttacking(false);
                    // try to attaq each monster if time from timer say it's been enough long time since the last attack)
                    for(Monster monster : wave.getMonsters()){
                        if(timer%(1000/t.getAttackSpeed())==0) {
                            if(!t.isAttacking()){
                                t.attack(monster,root);
                            }
                        }
                        // if isSpecial attacking true, make attack once again (will make special attack, only used by ragnar for multiple attack at same loop)
                        if (t.isSpecialAttacking()){
                            t.attack(monster,root);
                        }
                    }
                    // set special attack to false for the next iteration
                    t.setSpecialAttacking(false);
                }
                Iterator<Attack> iterAttack = getAttackList().iterator();
                // iterate on all the attacks animation, if size of movement lists > 1 animate else delete animation in attacklist et unrender it
                while (iterAttack.hasNext()){
                    Attack attack = iterAttack.next();
                    if (attack != null) {
                        if (attack.getDListX().size() > 1)
                            attack.attackMove();
                        else {
                            attack.unrender(root);
                            iterAttack.remove();
                        }
                    }
                }
                // set running true if user still have more than 0 lifes
                running = (inGameUser.getLifes()>0);
            }
            else if (!running){
                // update and save user stats
                // Clean alls the static variable to make the game replayable without closing it, then stop gameloop and launch menu
                Notification.showPopupMessage("Vous avez perdu à la manche "+wave.getLevel(), stage);
                user = new User();
                user.loadUser();
                user.addExp(0);
                user.addMoney((wave.getLevel()-1)*(wave.getLevel()-1));
                user.setLevelMax(Math.max(user.getLevelMax(),wave.getLevel()-1));
                user.addNumberGamesPlayed(1);
                user.addTotalWavesNumber(wave.getLevel()-1);
                user.saveUser();
                setPaused(true);
                attackList.clear();
                wave.getMonsters().clear();
                lt.clear();
                if (music.isPlaying()){
                    music.stop();
                }
                try{
                    stage.close();
                }
                catch (NullPointerException e){
                    e.printStackTrace();
                }
                Main menu = new Main();
                try {
                    menu.start(Main.getStage());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                gameLoop.stop();
                try {
                    pane3.getChildren().removeAll();
                    pane1.getChildren().removeAll();
                    root.getChildren().removeAll();
                    pane.getChildren().removeAll();
                    try {
                        scene.setRoot(null);
                    }
                    catch (NullPointerException e){
                        System.out.println("No more exception");
                    }
                    this.stop();
                    this.finalize();
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }
            }
        };
        // setup gameloop
        gameLoop = new Timeline(new KeyFrame(Duration.millis(33.3), gameUpdate));
        gameLoop.setCycleCount(Timeline.INDEFINITE);
        gameLoop.play();

        // setup panes of window
        pane3 = (BorderPane) FXMLLoader.load(Game.class.getResource("/Heroes/RightPanel/ListViewShop.fxml"));
        pane.setTop(pane1);
        pane.setRight(pane3);

        // final setup and show screen
        scene.setOnMouseClicked(mouseHandler);
        stage.setScene(scene);
        stage.show();

    }

    /**
     *  get all the attacks that have to be handled at this loop
      * @return the attacks
     */
    public static ArrayList<Attack> getAttackList() {
        return attackList;
    }

    /**
     * start
     * @param stage stage
     */
    public synchronized void start(Stage stage){
        running = true;
        stage.show();
    }


    /**
     * Buy tower
     *
     * @param map   the map of tiles
     * @param root  the root
     * @param stage the stage
     * @throws IOException the io exception
     */
    public void buyTower(MapOfTiles map, Pane root,Stage stage) throws IOException {
        Tower tower;
        // use towerkind to more quickly now the type (more simple and beatifuller than object equals in java)
        // create on object of this type
        if (towerselected != null){
            tower = new GreenLizard(mouse.getTileKey());

            if ((towerselected==TowerInfos.GREENLIZARD)){
                tower = new GreenLizard(mouse.getTileKey());
            }
            else if  ((towerselected==TowerInfos.LAZERMAN)){
                tower = new LazerMan(mouse.getTileKey());
            }
            else if  ((towerselected==TowerInfos.ULTRALADY)){
                tower = new UltraLady(mouse.getTileKey());
            }
            else if  ((towerselected==TowerInfos.CAPTAIN)){
                tower = new Captain (mouse.getTileKey());
            }
            else if  ((towerselected==TowerInfos.GOD)){
                tower = new God(mouse.getTileKey());
            }
            if ((mouse.getTileKey()>=0)&&(map.getTiles().get(mouse.getTileKey()).getTileKind() == TileKinds.CONSTRUCTIBLE)&& inGameUser.getMoney()>= tower.getBuyPrice()){
                // buy tower and place it, if conditions are validate (money and constructible tile)
                Notification.showPopupMessage("Tour acheté",stage);
                inGameUser.takeMoney(tower.getBuyPrice());
                System.out.println("Case constructible");
                lt.put(mouse.getTileKey(), tower);
                map.updateMap(mouse.getTileKey(),tower,map.getTiles());
                tower.setpos(mouse.getTileKey());
                map.render(mouse.getTileKey(), root);
            }
            else if (((mouse.getTileKey()>=0)&&(map.getTiles().get(mouse.getTileKey()).getTileKind() == TileKinds.CONSTRUCTIBLE))){
                // if not enough money to construct, make notif to tell it
                Notification.showPopupMessage("Vous n'avez pas asez d'argent",stage);
            }
            else if((mouse.getTileKey()>=0)&&(map.getTiles().get(mouse.getTileKey()).getTileKind() != TileKinds.NOT_CONSTRUCTIBLE)&&(map.getTiles().get(mouse.getTileKey()).getTileKind() != TileKinds.PATH)&&(map.getTiles().get(mouse.getTileKey()).getTileKind() != null)){
                // if a tower was clicked then show info about this tower in a pane that replace the right pane
                pane4 = (BorderPane) FXMLLoader.load(Game.class.getResource("/Heroes/RightPanel/TowerSelected.fxml"));
                pane.setRight(pane4);
            }
        }
    }

    /**
     * Animate and move (function used for gameloop).
     *
     * @param root  the root
     * @param path  the path
     * @param stage the stage
     */
    public void animateMove(Pane root, ArrayList<Integer> path, Stage stage) {
        // if all monsters of wave are death generate a message that new wave is coming, update wave number, and generate new wave
        if (wave.getMonsters().size() == 0) {
            inGameUser.setWaveNumber(wave.getLevel());
            wave =  new Wave(wave.getLevel(), path, root);
            Notification.showPopupMessage("Nouvelle vague en approche",stage);

        } else {
            // else for each monster animate if and move it to next element of his paths (x and y paths)
            for (int i = 0; i < wave.getMonsters().size(); i++) {
                Monster monster = wave.getMonsters().get(i);
                if (monster.getSizeOfListY()%4==0){
                    monster.animate(  monster.getSizeOfListX() % 2);
                }
                monster.monsterMove(0, (-i * 4) % (Tile.getTileSize() / 2));

                // if arrived, delete it of the list, unrender it and take the lifes to the user (monster value)
                if ((monster.getSizeOfListX() == 0)||(monster.getSizeOfListY()==0)) {
                    inGameUser.takeLifes(monster.getValue());
                    monster.unrender(root);
                    wave.getMonsters().remove(i);
                    break;
                }
                // if death, delete it of the list, unrender it and add money to user
                if (monster.getDeath()){
                    inGameUser.addMoney(monster.getValue() *30);
                    monster.unrender(root);
                    wave.getMonsters().remove(i);
                    monster.deathSound();
                    break;
                }
            }
        }
    }

    /**
     * Gets in game user.
     *
     * @return the in game user
     */
    public static InGameUser getInGameUser() {
        return inGameUser;
    }

    /**
     * Get user user.
     *
     * @return the user
     */
    public static User getUser(){ return user;}

    /**
     * Is paused boolean.
     *
     * @return the boolean
     */
    public static boolean isPaused() {
        return paused;
    }

    /**
     * Sets paused.
     *
     * @param paused the paused
     */
    public static void setPaused(boolean paused) {
        Game.paused = paused;
    }

    /**
     * Gets music.
     *
     * @return the music
     */
    public static Music getMusic() {
        return music;
    }

    /**
     * Sets music.
     *
     * @param music the music
     */
    public static void setMusic(Music music) {
        Game.music = music;
    }

    /**
     * Gets width.
     *
     * @return the width
     */
    public static int getWidth() {
        return width;
    }

    /**
     * Sets width.
     *
     * @param width the width
     */
    public static void setWidth(int width) {
        Game.width = width;
    }

    /**
     * Gets height.
     *
     * @return the height
     */
    public static int getHeight() {
        return height;
    }

    /**
     * Sets height.
     *
     * @param height the height
     */
    public static void setHeight(int height) {
        Game.height = height;
    }

    /**
     * Gets mouse.
     *
     * @return the mouse
     */
    public static Mouse getMouse() {
        return mouse;
    }

    /**
     * Sets mouse.
     *
     * @param mouse the mouse
     */
    public static void setMouse(Mouse mouse) {
        Game.mouse = mouse;
    }

    /**
     * Sets towerselected.
     *
     * @param towerselected the towerselected
     */
    public static void setTowerselected(TowerInfos towerselected) {
        Game.towerselected = towerselected;
    }

    /**
     * Gets map of towers (integer is tower key)
     *
     * @return the map of towers
     */
    public static HashMap<Integer, Tower> getLt() {
        return lt;
    }

    /**
     * Gets pane.
     *
     * @return the pane
     */
    public static BorderPane getPane() {
        return pane;
    }

    /**
     * Sets pane.
     *
     * @param pane the pane
     */
    public static void setPane(BorderPane pane) {
        Game.pane = pane;
    }

    /**
     * Gets pane 3.
     *
     * @return the pane 3
     */
    public static BorderPane getPane3() {
        return pane3;
    }

    /**
     * Gets pane 1.
     *
     * @return the pane 1
     */
    public static HBox getPane1() {
        return pane1;
    }


    /**
     * Gets map of tiles;=
     *
     * @return the map of tiles
     */
    public static MapOfTiles getMap() {
        return map;
    }

    /**
     * Gets root.
     *
     * @return the root
     */
    public static Pane getRoot() {
        return root;
    }
}
