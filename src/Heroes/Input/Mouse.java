package Heroes.Input;

import javafx.scene.input.MouseEvent;
import Heroes.Game;
import Heroes.Tile.Tile;

/**
 * Mouse handling
 * @author Nicolas
 */
public class Mouse {

    //VARIABLE
    /** stored x pos and y pos on screen*/
    private static double x,y;
    private static int button = -1;
    private static final int LEFT_BUTTON = 1;
    private static final int RIGHT_BUTTON = 3;
    private static final byte CODE = 0x2;
    /** mouse scale  */
    private static double scale = 1.0;

    //CONSTRUCTOR
    /**
     * Instatiate a new mouse
     * @param scale mouse scale
     */
    public Mouse(double scale) {
        Mouse.scale = scale;
    }


    //GETTER & SETTER
    /**
     * store mouse positionX
     * @param a mousePositionX
     */
    public void setX(double a){
        x = a;
    }
    /**
     * store mouse positionY
     * @param a mousePositionY
     */
    public void setY(double a){
        y = a;
    }


    /**
     * get position X (pixels)
     * @return x xPosition
     */
    public static double getX() {
        return x;
    }

    /**
     * get position Y (pixels)
     * @return y yPosition
     */
    public static double getY() {
        return y;
    }

    /**
     * get id (key) of a tile from the mouse position
     * @return key of the tile of mouse position
     */
    public int getTileKey(){
        // valeur par défauts
        int x = -1;
        int y = 0;
        // verifie que mouse est dans la zone de la map
        if ((Mouse.getY() > 50)&&(Mouse.getX() < Game.getWidth())){
            // Calcule x et y en tile
            x = (int) (Mouse.getX())/ Tile.getTileSize();
            y = (int) (Mouse.getY()-50)/ Tile.getTileSize();
        }
        // calcule la clé avec x et y en tile et la retourne
        return (y * (Game.getWidth() / Tile.getTileSize()) + x);
    }
}
