package Heroes.Map;

import Heroes.Exception.MapGenerationException;
import Heroes.Game;
import Heroes.Tile.Tile;
import Heroes.Tile.TileKinds;

import java.util.ArrayList;
import java.util.HashMap;

import static Heroes.Tile.TileKinds.CONSTRUCTIBLE;

/**
 * Class of mapGeneration
 *
 * @author Jordan Rey-Jouanchicot
 */
public class MapGenerator{

    // Define the number of min and max of constructible tiles
    private static final int minNumberOfConstructible = (int) (50 *( (float) Tile.getTileOriginalSize()/Tile.getTileSize())*((float) Tile.getTileOriginalSize()/Tile.getTileSize()));
    private static final int maxNumberOfConstructible = (int) (100 *( (float) Tile.getTileOriginalSize()/Tile.getTileSize())*((float) Tile.getTileOriginalSize()/Tile.getTileSize()));

    // Define the value used to choose the path, more the value of constructible tile is compared to the end one, more the path will try to go near to constructible tiles
    private static final int valueOfEndPoint = 100;
    private static final int valueOfTileConstructible = 100;

    // Map of tiles key are encoded x and y in tile, tilekind is the type of the tile
    private HashMap<Integer, TileKinds> tilesKindMap = new HashMap<>();

    // Arraylist of path key
    private ArrayList<Integer> setMRanged;


    /**
     * Gets map of tilesKind .
     *
     * @return the tiles kind map
     */
    public HashMap<Integer, TileKinds> getTilesKindMap() {
        return tilesKindMap;
    }

    /**
     * Gets path key.
     *
     * @return the setMranged
     */
    public ArrayList<Integer> getSetMRanged() {
        return setMRanged;
    }

    /**
     * Generate a map
     *
     * @throws MapGenerationException the map generation exception
     */
    public void gen() throws MapGenerationException {
        // Choix aléatoire du y de début et de fin
        int yBeginning = (int) (Math.random() * (Game.getHeight()/Tile.getTileSize()-1));
        int yEndOfPath = (int) (Math.random() * (Game.getHeight()/Tile.getTileSize()-1));
        int x;
        int y;
        int numberOfTiles;

        numberOfTiles =  (int) (Math.random()*(MapGenerator.maxNumberOfConstructible - MapGenerator.minNumberOfConstructible))+minNumberOfConstructible;

        // Creation de l'ensemble des cases constructiblle
        for (int i =1;i<numberOfTiles-1;i++){
            x = (int) (Math.random() * (Game.getWidth()/Tile.getTileSize()-1));
            y = (int) (Math.random() * (Game.getHeight()/Tile.getTileSize()-1));
            tilesKindMap.put(y*(Game.getWidth()/Tile.getTileSize())+x, CONSTRUCTIBLE);
        }

        int xStart = 0;
        int yStart = yBeginning;
        int key;
        int xNext = 0;
        int yNext = yBeginning;

        // Ajoute ENSM
        setMRanged = new ArrayList<>();

        tilesKindMap.put(yStart*(Game.getWidth()/Tile.getTileSize())+xStart, TileKinds.PATH);
        setMRanged.add(yStart*(Game.getWidth()/Tile.getTileSize())+xStart);

        int counterInfiniteLoop = 0;
        // Boucle en tentant de creer un chemin
        while (((yNext != yEndOfPath)||(xNext != (Game.getWidth()/Tile.getTileSize()-1)))&&(counterInfiniteLoop <= (Game.getWidth()/Tile.getTileSize())*(Game.getHeight()/Tile.getTileSize()))){
            counterInfiniteLoop++;
            // Si trop d'itération (plus que la taille de la map, génére un exception et vide les listes et map
            if (counterInfiniteLoop >= (Game.getWidth()/Tile.getTileSize())*(Game.getHeight()/Tile.getTileSize())){
                setMRanged.clear();
                tilesKindMap.clear();
                throw new MapGenerationException();
            }
            double maxValue = 0;
            int j;
            int jMin;
            int jMax;
            int maxKey=0;
            // Calcule value des cases connectées (4) et prend le max comme valeur suivante du chemin
            for (int i =-1;i <=1;i++ ) {
                if ((i == -1)||(i == 1)){
                    jMin = 0;
                    jMax = 0;
                }
                else{
                    jMin = -1;
                    jMax = 1;
                }
                for (j = jMin;j <=jMax;j++ ) {
                    if (((yStart+j)>0)&&((xStart+i)>0)&&((yStart+j)<Game.getHeight()/Tile.getTileSize())&&((xStart+i)<Game.getWidth()/Tile.getTileSize())){
                        key = ((yStart+j)*Game.getWidth()/Tile.getTileSize())+(xStart+i);
                        if (((value(key,yEndOfPath)> maxValue)&&(!(tilesKindMap.containsKey(key))))&&(!(setMRanged.contains(key)))){
                            maxValue = value(key,yEndOfPath);
                            maxKey = key;
                            yNext = yStart+j;
                            xNext = xStart+i;
                        }
                    }
                }
            }
            // On met a jour la valeur a la map si le système n'a pas encore trop d'itérations et ajoute a la liste du chemin la clé
            if (counterInfiniteLoop <= (Game.getWidth()/Tile.getTileSize())*(Game.getHeight()/Tile.getTileSize())) {
                // System.out.println("xNext , yNext"+ xNext + " "+yNext);
                yStart = yNext;
                xStart = xNext;
                tilesKindMap.put(maxKey,TileKinds.PATH);
                setMRanged.add(maxKey);
            }
        }
        //On a met a jour la valeur a la map si le système n'a pas encore trop d'itérations et ajoute a la liste du chemin la clé (dernière valeur (yEndOfPath))
        if (counterInfiniteLoop <= (Game.getWidth()/Tile.getTileSize())*(Game.getHeight()/Tile.getTileSize())) {
            tilesKindMap.put(yEndOfPath * (Game.getWidth() / Tile.getTileSize()) + (Game.getWidth() / Tile.getTileSize() - 1), TileKinds.PATH);

            setMRanged.add(yEndOfPath * (Game.getWidth() / Tile.getTileSize()) + (Game.getWidth() / Tile.getTileSize() - 1));
        }
    }


    /**
     * Calculate value of a Tile
     * @param xy key
     * @param yEndOfPath yArrive
     * @return calculate value
     */
    private double value(int xy, int yEndOfPath){
        // for each tile where x > x of actual key, and of kind tile constructibles,  calculate value of actual tile
        double value = this.tilesKindMap
                .keySet()
                .stream()
                .filter(x -> (xy % (Game.getWidth()/Tile.getTileSize()))> (x % (Game.getWidth()/Tile.getTileSize())))
                .filter(key -> this.tilesKindMap.get(key) == CONSTRUCTIBLE)
                .mapToInt(x -> valueOfTileConstructible/manhattanDistance(xy, x)).sum();
        value += (double) valueOfEndPoint/manhattanDistance(yEndOfPath*(Game.getWidth()/Tile.getTileSize())+(Game.getWidth()/Tile.getTileSize()-1),xy);
        return  (value);
    }

    /**
     * Manhattan distance
     * @param xy key of one tile
     * @param xy1 key of a second tile
     * @return manhattanDistances
     */
    private int manhattanDistance(int xy, int xy1) {
        // Calculate x and y from first key
        int x = xy % (Game.getWidth() / Tile.getTileSize());
        int y = (xy - x) / (Game.getWidth() / Tile.getTileSize());


        // Calculate x and y from second key
        int x1 = xy1 % (Game.getWidth() / Tile.getTileSize());
        int y1 = (xy1 - y) / (Game.getWidth() / Tile.getTileSize());
        // calculate and return manhattan distance, if manhattan distance =0, return 1
        int res = (Math.abs(y - y1) + Math.abs(x - x1));
        if (res == 0) {
            res = 1;
        }
        return (res);
    }
}
