package Heroes.Map;

import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import Heroes.Exception.MapGenerationException;
import Heroes.Game;
import Heroes.Tile.Tile;
import Heroes.Tile.TileKinds;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

/**
 * MapOfTile class to store object and create map
 *
 * @author Jordan Rey-Jouanchicot
 */
public class MapOfTiles extends HashMap {
    /**
     * widhShift variable to place things more nicely
     */
    private static final int widthShift = 37;
    /**
     * heightShift variable to place
     */
    private static final int heightShift = Tile.getTileSize() /2;

    /**
     * map of tiles
     */
    private HashMap<Integer,Tile> tiles = new HashMap<>();
    /**
     * list of all path key ordered
     */
    private ArrayList<Integer> pathList = new ArrayList<>();

    /**
     * Gets width shift.
     *
     * @return the width shift
     */
    public static int getWidthShift() {
        return widthShift;
    }

    /**
     * Gets height shift.
     *
     * @return the height shift
     */
    public static int getHeightShift() {
        return heightShift;
    }

    /**
     * Create hash map with all values to not constructible and with key (xy encoded) at index
     * @return the hash map
     */
    public HashMap<Integer,Tile> createMap(){
        Integer i;
        Integer j;
        // Initialise each tile of map with a value not constructible
        for (j = 0; j < Game.getHeight() / Tile.getTileSize(); j += 1) {
            for (i = 0; i < Game.getWidth() / Tile.getTileSize(); i += 1) {
                tiles.put(j * (Game.getWidth() / Tile.getTileSize()) + i, new Tile(i,j, TileKinds.NOT_CONSTRUCTIBLE));
            }
        }
        return tiles;
    }

    /**
     * Update the hashmap of tiles
     * @return the hash map
     */
    public HashMap<Integer,Tile> updateMap(){
        // Try to make a map path, if it does not succeed, try again until success
        boolean notGenerated = true;
        MapGenerator mg = new MapGenerator();
        do {
            try {
                mg.gen();
                notGenerated = false;
            }
            catch(MapGenerationException mapEx){
                System.out.println(mapEx);
            }
        } while (notGenerated);
        // update tiles kind from tilekind map
        mg.getTilesKindMap().forEach((key, value) -> tiles.get(key).setTileKind(value));
        // store list of tile key to make path for monsters
        pathList = mg.getSetMRanged();
        return tiles;
    }

    /**
     * Render map.
     *
     * @param root the root
     */
    public void renderMap(Pane root){
        // Render each tile of map not path, and path tiles then
        tiles.values().forEach(x->x.render(root));
        pathList.forEach(x ->renderOnePath(root,x));
    }


    /**
     * Render one path.
     *@author Jordan and Vincent
     * @param root the root
     * @param key  the key
     */
    public void renderOnePath(Pane root, int key){
        int index = pathList.indexOf(key);
        int size = pathList.size();
        // get nex key and previous key and use it to calculate tile shape
        if ((index >0)&&(index<size-2)){
            int nextKey = pathList.get(pathList.indexOf(key)+1);
            int previousKey = pathList.get(pathList.indexOf(key)-1);
            render(root, nextKey, previousKey,key);
        } else if (index == 0) {
            int nextKey = pathList.get(pathList.indexOf(key)+1);
            render(root, nextKey, key-1, key);
        }
        else {
            unrender(key,root);
            Tile tile = tiles.get(key);
            int previousKey = pathList.get(pathList.indexOf(key)-1);
            int xPrev = previousKey % (Game.getWidth() / Tile.getTileSize());
            int yPrev = (previousKey-xPrev)/(Game.getWidth() / Tile.getTileSize());

            int x = key % (Game.getWidth() / Tile.getTileSize());
            int y = (key-x)/(Game.getWidth() / Tile.getTileSize());

            if (Tile.getRan() <1) {
                if (xPrev - x == -1) tile.render(root, "/assets/map/Paths/pathGD.png");
                if (yPrev - y == 1) tile.render(root, "/assets/map/Paths/pathBD.png");
                if (yPrev - y == -1) tile.render(root, "/assets/map/Paths/pathHD.png");
            }
            else if (Tile.getRan() <2) {
                if (xPrev - x == -1) tile.render(root, "/assets/map2/road/roadGD.png");
                if (yPrev - y == -1) tile.render(root, "/assets/map2/road/roadHD.png");
                if (yPrev - y == 1) tile.render(root, "/assets/map2/road/roadBD.png");
            }
        }
    }

    /**
     * Render path
     * @author Jordan and Vincent
     * @param root        the root
     * @param nextKey     the next key
     * @param previousKey the previous key
     * @param key         the key
     */
    public void render(Pane root, int nextKey, int previousKey, int key){
        Tile tile = tiles.get(key);
        int x = key % (Game.getWidth() / Tile.getTileSize());
        int y = (key-x)/(Game.getWidth() / Tile.getTileSize());

        // X et y, x et y prev de key et prev key
        int xPrev = previousKey % (Game.getWidth()/ Tile.getTileSize());
        int yPrev = (previousKey-xPrev)/(Game.getWidth()/ Tile.getTileSize());

        int xNext = nextKey % (Game.getWidth() / Tile.getTileSize());
        int yNext = (nextKey - xNext) / (Game.getWidth() / Tile.getTileSize());

        // calculer forme des cases générales
        // cas premier design
        if (Tile.getRan() <1) {
            if ((xPrev - x == -1) && (yPrev - y == 0)) { // AVANT
                if (((x - xNext == -1)) && (y - yNext == 0)) {
                    tile.render(root, "/assets/map/Paths/pathGD.png");
                } else if ((x - xNext == 0) && (y - yNext == 1)) {
                    tile.render(root, "/assets/map/Paths/pathHG.png");
                } else if ((x - xNext == 0) && (y - yNext == -1)) {
                    tile.render(root, "/assets/map/Paths/pathBG.png");
                }
            } else if ((yPrev - y == -1) && (xPrev - x == 0)) { //HAUT
                if (((x - xNext == 1)) && (y - yNext == 0)) {
                    tile.render(root, "/assets/map/Paths/pathHG.png");
                } else if ((x - xNext == 0) && (y - yNext == -1)) {
                    tile.render(root, "/assets/map/Paths/pathHB.png");
                } else if ((x - xNext == -1) && (y - yNext == 0)) {
                    tile.render(root, "/assets/map/Paths/pathHD.png");
                }
            } else if ((xPrev - x == 1) && (yPrev - y == 0)) { // ARRI7RE
                if (((x - xNext == 1)) && (y - yNext == 0)) {
                    tile.render(root, "/assets/map/Paths/pathGD.png");
                } else if ((x - xNext == 0) && (y - yNext == 1)) {
                    tile.render(root, "/assets/map/Paths/pathHD.png");
                } else if ((x - xNext == 0) && (y - yNext == -1)) {
                    tile.render(root, "/assets/map/Paths/pathBD.png");
                }
            } else if ((yPrev - y == 1) && (xPrev - x == 0)) { //BAS
                if (((x - xNext == 1)) && (y - yNext == 0)) {
                    tile.render(root, "/assets/map/Paths/pathBG.png");
                } else if ((x - xNext == 0) && (y - yNext == 1)) {
                    tile.render(root, "/assets/map/Paths/pathHB.png");
                } else if ((x - xNext == -1) && (y - yNext == 0)) {
                    tile.render(root, "/assets/map/Paths/pathBD.png");
                }
            } else {
                tile.render(root, "/assets/map/Paths/pathGD.png");
            }
        }
        else { // cas  design 2
            if ((xPrev - x == -1) && (yPrev - y == 0)) { // AVANT
                if (((x - xNext == -1)) && (y - yNext == 0)) {
                    tile.render(root, "/assets/map2/road/roadGD.png");
                } else if ((x - xNext == 0) && (y - yNext == 1)) {
                    tile.render(root, "/assets/map2/road/roadHG.png");
                } else if ((x - xNext == 0) && (y - yNext == -1)) {
                    tile.render(root, "/assets/map2/road/roadBG.png");
                }
            } else if ((yPrev - y == -1) && (xPrev - x == 0)) { //HAUT
                if (((x - xNext == 1)) && (y - yNext == 0)) {
                    tile.render(root, "/assets/map2/road/roadHG.png");
                } else if ((x - xNext == 0) && (y - yNext == -1)) {
                    tile.render(root, "/assets/map2/road/roadHB.png");
                } else if ((x - xNext == -1) && (y - yNext == 0)) {
                    tile.render(root, "/assets/map2/road/roadHD.png");
                }
            } else if ((xPrev - x == 1) && (yPrev - y == 0)) { // ARRI7RE
                if (((x - xNext == 1)) && (y - yNext == 0)) {
                    tile.render(root, "/assets/map2/road/roadGD.png");
                } else if ((x - xNext == 0) && (y - yNext == 1)) {
                    tile.render(root, "/assets/map2/road/roadHD.png");
                } else if ((x - xNext == 0) && (y - yNext == -1)) {
                    tile.render(root, "/assets/map2/road/roadBD.png");
                }
            } else if ((yPrev - y == 1) && (xPrev - x == 0)) { //BAS
                if (((x - xNext == 1)) && (y - yNext == 0)) {
                    tile.render(root, "/assets/map2/road/roadBG.png");
                } else if ((x - xNext == 0) && (y - yNext == 1)) {
                    tile.render(root, "/assets/map2/road/roadHB.png");
                } else if ((x - xNext == -1) && (y - yNext == 0)) {
                    tile.render(root, "/assets/map2/road/roadBD.png");
                }
            } else {
                tile.render(root, "/assets/map2/road/roadGD.png");
            }
        }
    }

    /**
     * Update hash map of tiles.
     *
     * @param key  the key
     * @param tile the tile
     * @param hm   the hm
     * @return the hash map
     */
    public  HashMap<Integer,Tile> updateMap(int key,Tile tile, HashMap<Integer,Tile> hm){
        hm.put(key, tile);
        return hm;
    }

    /**
     * Render a case and rerender case over it to avoid texture issue
     *
     * @param key  the key
     * @param root the root
     */
    public void render(int key,Pane root){
        this.tiles.get(key).render(root);
        renderOverTile(key, root);
    }

    /**
     * Render
     * @param key the key
     * @param root the root
     * @param ress the ressource to set (image)
     */
    public void render(int key, Pane root, String ress){
        this.tiles.get(key).render(root,ress);
        renderOverTile(key, root);
    }
    /**
     * Render a case and rerender case over it to avoid texture issue
     *
     * @param key  the key
     * @param root the root
     */
    public void renderOverTile(int key, Pane root){
        TileKinds tk = this.tiles.get(key+Game.getWidth() / Tile.getTileSize()).getTileKind();
        // Render the case under it
        if ((tk!= TileKinds.NOT_CONSTRUCTIBLE)&&(tk!= TileKinds.CONSTRUCTIBLE)&&(tk!= TileKinds.PATH)&&(this.tiles.get(key + (Game.getWidth() / Tile.getTileSize())) != null)){
            this.tiles.get(key + (Game.getWidth() / Tile.getTileSize())).unrender(root);
            this.tiles.get(key + (Game.getWidth() / Tile.getTileSize())).render(root);
        }
    }

    /**
     * Unrender.
     *
     * @param key  the key
     * @param root the root
     */
    public void unrender(int key,Pane root){
        this.tiles.get(key).unrender(root);
    }

    /**
     * Gets path list.
     *
     * @return the path list
     */
    public ArrayList<Integer> getPathList() {
        return pathList;
    }

    /**
     * Gets tiles hashmap.
     *
     * @return the tiles
     */
    public HashMap<Integer, Tile> getTiles() {
        return tiles;
    }
}
