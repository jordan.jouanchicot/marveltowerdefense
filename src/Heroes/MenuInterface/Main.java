package Heroes.MenuInterface;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import Heroes.User.User;

import java.awt.*;
import java.net.URL;

/**
 * Class of menus
 *
 * @author Vincent
 */
public class Main extends Application {

    /*** stage Window*/
    private static Stage stage;
    /** User */
    private static User user;
    /** Map layout*/
    private static Parent root;


    /**
     * Gets user.
     *
     * @return the user
     */
    public static User getUser() {
        return user;
    }

    /**
     * Sets user.
     *
     * @param user the user
     */
    public static void setUser(User user) {
        Main.user = user;
    }

    /**
     * Gets root layout.
     *
     * @return the root
     */
    public static Parent getRoot() {
        return root;
    }

    /**
     * Method start a menu
     * @param primaryStage stage
     * @throws Exception throw it
     */
    @Override
    public void start(Stage primaryStage) throws Exception{
        user = new User();
        Parent root;
        // If user loaded correctly show user menu connected else show first game menu
        if (user.loadUser()){
             root = FXMLLoader.load(getClass().getResource("/Heroes/MenuInterface/menuPlayer.fxml"));
        }
        else {
            root = FXMLLoader.load(getClass().getResource("/Heroes/MenuInterface/menu.fxml"));
        }
        stage = primaryStage;

       // Save when close window request
        Main.getStage().setOnCloseRequest(event -> {
            System.out.println("Stage is closing");
            user.saveUser();
        });

        // Show menu
        primaryStage.getIcons().add(new javafx.scene.image.Image(Main.class.getResourceAsStream("/assets/logo/logo.png")));
        primaryStage.setResizable(false);
        primaryStage.setTitle("Heroes tower defense");
        primaryStage.setScene(new Scene(root, 1400, 700));
        primaryStage.show();
    }


    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Get stage stage.
     *
     * @return the stage
     */
    public static Stage getStage(){
        return stage;
    }
}
