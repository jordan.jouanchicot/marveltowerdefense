package Heroes.MenuInterface;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import Heroes.Exception.RessourceNotFoundException;
import Heroes.Exception.StageException;
import Heroes.Game;
import Heroes.User.User;

import java.io.IOException;

/**
 * The type Menu controller.
 */
public class MenuController{
    /**
     * The Quit button.
     */
    @FXML Button quitButton;
    /**
     * The Play button.
     */
    @FXML Button playButton;
    /**
     * The Credit button.
     */
    @FXML Button creditButton;
    /**
     * The Settings button.
     */
    @FXML Button settingsButton;
    /**
     * The Pseudo.
     */
    @FXML TextField pseudo;

    //permet de quitter le je jeu en cliquant sur le bouton Quitter

    /**
     * On mouse clicked quit.
     */
    public void onMouseClickedQuit(){
        Main.getStage().close();
    }

    //permet de jouer après avoir commuiquer son speudonyme
    /**
     * On mouse clicked play if pseudony entered
     * call the function Call and treat the exception
     */
    public void onMouseClickedPlay(){
        try{
            onMouseClickedPlayCall();
        }
        catch (StageException ex){
            System.out.println(ex);
            Platform.exit();
        }
    }

    /**
     * On mouse clicked play call.
     *
     * @throws StageException the stage exception
     */
    public void onMouseClickedPlayCall() throws StageException{
        if (pseudo.getText().length()>0){
            Main.setUser(new User());
            Main.getUser().readNewUser(pseudo.getText());
            Main.getUser().saveUser();
            Game game = null;
            try {
                game = new Game(Main.getStage());
            } catch (IOException e) {
                throw new StageException();
            }
            game.start(Main.getStage());
        }
    }


    //permet de voir les crédits du jeu
    /**
     * On mouse clicked credit of games appears.
     */
    public void onMouseClickedCredit(){
        try{
            onMouseClickedCreditCall();
        }
        catch (RessourceNotFoundException e){
            System.out.println(e);
            Platform.exit();
        }
    }

    /**
     * On mouse clicked credit call.
     *
     * @throws RessourceNotFoundException the ressource not found exception
     */
    public void onMouseClickedCreditCall() throws RessourceNotFoundException{
        try{
            Parent root = FXMLLoader.load(getClass().getResource("/Heroes/MenuInterface/Credit.fxml"));
            Stage stage = new Stage();
            stage.setScene(new Scene(root,800,450));
            stage.show();
        }
        catch (Exception e){
            throw new RessourceNotFoundException();
        }
    }


    //permet d'accédé aux paramètres du jeu
    /**
     * On mouse clicked settings.
     */
    public void onMouseClickedSettings(){
        Settings settings;
        settings = new Settings();
        Stage stage = new Stage();
        settings.start(stage);
    }
}
