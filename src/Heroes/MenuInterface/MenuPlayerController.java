package Heroes.MenuInterface;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import Heroes.Exception.RessourceNotFoundException;
import Heroes.Exception.StageException;
import Heroes.Game;
import Heroes.User.User;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * The type Menu player controller.
 */
public class MenuPlayerController implements Initializable {
    /**
     * The Play button.
     */
    @FXML Button playButton;
    /**
     * The Credit button.
     */
    @FXML Button creditButton;
    /**
     * The Tower improvement.
     */
    @FXML Button towerImprovement;
    /**
     * The Settings button.
     */
    @FXML Button settingsButton;
    /**
     * The Text pseudo.
     */
    @FXML Label textPseudo;
    /**
     * The Stat 1 label.
     */
    @FXML Label stat1;
    /**
     * The Stat 2 label.
     */
    @FXML Label stat2;
    /**
     * The Stat 3 label.
     */
    @FXML Label stat3;
    /**
     * The Stat 4 label.
     */
    @FXML Label stat4;

    /**
     * Override initialize and show value of users stats
     * @param url url
     * @param resourceBundle ressourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        Main.setUser(new User());
        Main.getUser().loadUser();
        textPseudo.setText("Bonjour "+ Main.getUser().getName()+",");
        stat1.setText("Argent : "+ Main.getUser().getMoney());
        stat2.setText("Vague maximale : "+ Main.getUser().getLevelMax());
        stat3.setText("Vagues totales : "+ Main.getUser().getTotalWavesNumber());
        stat4.setText("Nombre de parties totales : "+ Main.getUser().getNumberGamesPlayed());
    }

    /**
     * On mouse clicked play.
     */
    //permet de jouer après avoir commuiquer son speudonyme
    public void onMouseClickedPlay(){
        try{
            onMouseClickedPlayCall();
        }
        catch (StageException ex){
            System.out.println(ex);
            Platform.exit();
        }
    }

    /**
     * On mouse clicked play call.
     *
     * @throws StageException the stage exception
     */
    public void onMouseClickedPlayCall() throws StageException{
        // close main game
        Main.getStage().close();
        // Try to start game
        Game game;
        try {
            game = new Game(Main.getStage());
            game.start(Main.getStage());
        } catch (IOException e) {
            throw new StageException();
        }
        game.start(Main.getStage());
    }


    /**
     * On mouse clicked credit.
     */
    //permet de voir les crédits su jeu
    public void onMouseClickedCredit(){
        try{
            onMouseClickedCreditCall();
        }
        catch (RessourceNotFoundException e){
            System.out.println(e);
            Platform.exit();
        }
    }

    /**
     * On mouse clicked credit call.
     *
     * @throws RessourceNotFoundException the ressource not found exception
     */
    public void onMouseClickedCreditCall() throws RessourceNotFoundException{
        // Try to show credit window
        try{
            Parent root = FXMLLoader.load(getClass().getResource("/Heroes/MenuInterface/Credit.fxml"));
            Stage stage = new Stage();
            stage.setScene(new Scene(root,800,450));
            stage.show();
        }
        catch (Exception e){
            throw new RessourceNotFoundException();
        }
    }

    /**
     * On mouse clicked towers improvements.
     */
    public void onMouseClickedTowersImprovements(){
        System.out.println("Clicked");
        // try to launch tower improvement settings and its screen
        try{
            Parent root = FXMLLoader.load(getClass().getResource("/Heroes/MenuInterface/TowerImprovementInterface.fxml"));
            Stage stage = new Stage();
            stage.setOnCloseRequest(event -> {
                System.out.println("Stage is closing");
                Main.getUser().saveUser();
            });
            stage.setScene(new Scene(root,1200,800));
            stage.show();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * On mouse clicked settings.
     */
    //permet d'accédé aux paramètres du jeu
    public void onMouseClickedSettings(){
        Settings settings;
        settings = new Settings();;
        Stage stage = new Stage();
        settings.start(stage);
    }
}
