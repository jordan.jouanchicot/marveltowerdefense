package Heroes.MenuInterface;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import Heroes.Game;
import Heroes.Tile.Tile;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;

/**
 * The class Settings used to store settings.
 * @author Etienne
 */
public class Settings extends Application {
    /** TileSize */
    @Expose private int tileSize;

    /** width */
    @Expose private int width;

    /**
     * Start method
     * @param stage stage
     */
    @Override
    public void start(Stage stage) {
        // initialize settings layout et show it
        Pane root = null;
        try {
            root = FXMLLoader.load(Game.class.getResource("/Heroes/MenuInterface/Settings.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        Image fond = new Image("/assets/menu/fond.jpg", true);


        BackgroundImage back = new BackgroundImage(fond,
                BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER,
                new BackgroundSize(1.0, 1.0, true, true, false, false));

        root.setBackground(new Background(back));

        stage.setResizable(false);
        Scene scene = new Scene(root, 1000, 500);
        stage.setScene(scene);
        stage.setTitle("Paramètres");
        scene.setRoot(root);
        stage.show();
    }


    /**
     * Load settings return if load from file was correct.
     *
     * @return the boolean
     */
    public boolean loadSettings(){
        boolean bool;
        Settings settingsLoaded;
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        // Try to read file, if file is correct load settings from it
        File file = new File("src/Heroes/settings.json");
        try {
            Reader reader = Files.newBufferedReader(file.toPath().toRealPath());
            settingsLoaded = gson.fromJson(reader, Settings.class);
            System.out.println("File is correct");
            Game.setWidth(settingsLoaded.width);
            Game.setHeight(settingsLoaded.width / 16 * 9);
            Tile.setTileSize(settingsLoaded.tileSize);
            bool = true;
        }
        catch (IOException| NullPointerException e){
            System.out.println("File is not filled correctly");
            bool = false;
        }
        return bool;
    }

    /**
     * Instantiates a new Settings with defaults values.
     */
    public Settings() {
        this.tileSize = 60;
        this.width = 1200;
    }


    /**
     * Save settings in file.
     */
    public void saveSettings(){
        // try to Update saved value to settings
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        File file = new File("src/Heroes/settings.json");
        try {
            FileWriter write = new FileWriter(file);
            gson.toJson(this, write);
            write.close();
            System.out.println("Saved done !");
        }
        catch (IOException e){
            System.out.println(e);
        }
    }

    /**
     * Gets tile size.
     *
     * @return the tile size
     */
    public int getTileSize() {
        return tileSize;
    }

    /**
     * Sets tile size.
     *
     * @param tileSize the tile size
     */
    public void setTileSize(int tileSize) {
        this.tileSize = tileSize;
    }

    /**
     * Gets width.
     *
     * @return the width
     */
    public int getWidth() {
        return width;
    }

    /**
     * Sets width.
     *
     * @param width the width
     */
    public void setWidth(int width) {
        this.width = width;
    }
}