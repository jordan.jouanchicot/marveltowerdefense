package Heroes.MenuInterface;

import Heroes.Notification.Notification;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;

import java.net.URL;
import java.util.ResourceBundle;


/**
 * The type Settings controller.
 * @author Etienne
 */
public class SettingsController implements Initializable {

    /**
     * The first radiobutton (first setting).
     */
    @FXML RadioButton bt1;
    /**
     * The second radiobutton (first setting).
     */
    @FXML RadioButton bt2;
    /**
     * The third radiobutton (first setting).
     */
    @FXML RadioButton bt3;
    /**
     * The third radiobutton (second setting).
     */
    @FXML RadioButton bt3c;
    /**
     * The first radiobutton (second setting).
     */
    @FXML RadioButton bt1c;
    /**
     * The second radiobutton (second setting).
     */
    @FXML RadioButton bt2c;

    /**
     * The Button apply.
     */
    @FXML
    Button button;

    /**
     * Initialize at default value button selected
     * @param url url
     * @param resourceBundle ressourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        final ToggleGroup sizeScreen = new ToggleGroup();

        final ToggleGroup sizeTile= new ToggleGroup();

        bt1.setToggleGroup(sizeScreen);
        bt2.setToggleGroup(sizeScreen);
        bt3.setToggleGroup(sizeScreen);
        bt1.setSelected(true);

        bt1c.setToggleGroup(sizeTile);
        bt2c.setToggleGroup(sizeTile);
        bt3c.setToggleGroup(sizeTile);
        bt2c.setSelected(true);
    }

    /**
     * Apply button action
     * change values
     * save the settings and leave
     */
    @FXML
    private void OnClickedApply(){
        Settings settings = new Settings();
        if (bt1.isSelected()) {
            //METHODE ECRAN PETIT
            settings.setWidth(1200);
            System.out.println("Ecran petit");
        } else if (bt2.isSelected()){
            //METHODE Ecran moyen
            settings.setWidth(1400);
            System.out.println("Ecran moyennes");
        }
        else {
            //METHODE ECRAN GRAND
            settings.setWidth(1700);
            System.out.println("Ecran grand");
        }
        if (bt1c.isSelected()) {
            //METHODE Tuile PETIT
            settings.setTileSize(40);
            System.out.println("Tuiles petites");
        } else if (bt2c.isSelected()){
            //METHODE Tuile moyen
            settings.setTileSize(60);
            System.out.println("Tuiles moyennes");
        }
        else{
            settings.setTileSize(80);
            System.out.println("Tuiles grandes");
        }
        if (bt3.isSelected()&&bt1c.isSelected()){
            Notification.showPopupMessage("Cas impossible", Main.getStage());
            bt2c.setSelected(true);
            settings.setTileSize(60);
        }
        else if (bt2.isSelected()&&bt1c.isSelected()){
            Notification.showPopupMessage("Cas impossible", Main.getStage());
            bt2c.setSelected(true);
            settings.setTileSize(60);
        }
        settings.setTileSize((settings.getWidth()/(1200/settings.getTileSize())));
        settings.saveSettings();
        Platform.exit();
    }
}
