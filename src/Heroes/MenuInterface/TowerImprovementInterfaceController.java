package Heroes.MenuInterface;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import Heroes.Notification.Notification;
import Heroes.Tile.TowerInfos;
import Heroes.User.TowerImprovement;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static Heroes.User.TowerImprovement.*;

/**
 * The type Tower improvement interface controller.
 */
public class TowerImprovementInterfaceController implements Initializable {

    /**
     * The List view of towerImprovement.
     */
    @FXML ListView<TowerImprovement> listView;
    private ObservableList<TowerImprovement> towerlist ;
    /**
     * The Label.
     */
    @FXML Label label;
    /**
     * The Money label.
     */
    @FXML Label moneyLabel;
    /**
     * The Attack btn to up it.
     */
    @FXML Button attackBtn;
    /**
     * The Attack range btn to up it.
     */
    @FXML Button attackRangeBtn;
    /**
     * The Attack rate btn to up it.
     */
    @FXML Button attackRateBtn;


    /**
     * Instantiates a new Tower improvement interface controller.
     */
    public TowerImprovementInterfaceController() {
        towerlist = FXCollections.observableArrayList();

        ArrayList<TowerInfos> towerlistInfos = new ArrayList<>();
        towerlistInfos.addAll(Stream.of(TowerInfos.values()).collect(Collectors.toList()));

        towerlistInfos.forEach(x -> towerlist.add(Main.getUser().getTowersImprovements().towerImprovements(x)));
    }

    /**
     * Gets selected tower.
     *
     * @return the selected tower
     */
    public TowerImprovement getSelectedTower() {
      return  (listView.getSelectionModel().getSelectedItem());
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        listView.setItems(towerlist);
        label.setText("\n Arbre technologique");
        moneyLabel.setText("Argent restant = "+(Main.getUser().getMoney()));
        attackRangeBtn.setText("Améliorer portée attaque pour "+ getCostAttackRange() +"$");
        attackRateBtn.setText("Améliorer fréquence attaque pour "+ getCostAttackRate() +"$");
        attackBtn.setText("Améliorer force d'attaque pour "+ getCostAttack() +"$");
        listView.setCellFactory(towerInfosListView -> new TowerImprovementListViewCell());


        // Pour chaque bouton definir onclick application de l'action sur selected tower puis appeler initialize pour mettre a jour les valeurs
        attackBtn.setOnMouseClicked(mouseEvent -> {
            if ((getSelectedTower().getAttack()/ getValAttack() < getMaxNumberOfEach())&&(getCostAttack() < Main.getUser().getMoney())&&(getSelectedTower()!=null)){
                Notification.showPopupMessage("Amélioration efféctuée !",Main.getStage());
                Main.getUser().addMoney(-getCostAttack());
                getSelectedTower().setAttack(getSelectedTower().getAttack()+ getValAttack());
                initialize(url,resourceBundle);
            }
            else {
                Notification.showPopupMessage("Vous ne pouvez effectuer cette amélioration",Main.getStage());
            }
        });

        attackRangeBtn.setOnMouseClicked(mouseEvent -> {
            if ((getSelectedTower().getAttackRange()/ getValAttackRange() < getMaxNumberOfEach())&&(getCostAttackRange() < Main.getUser().getMoney())&&(getSelectedTower()!=null)){
                Notification.showPopupMessage("Amélioration efféctuée !",Main.getStage());
                Main.getUser().addMoney(-getCostAttackRange());
                getSelectedTower().setAttackRange(getSelectedTower().getAttackRange()+ getValAttackRange());
                initialize(url,resourceBundle);
            }
            else {
                Notification.showPopupMessage("Vous ne pouvez effectuer cette amélioration",Main.getStage());
            }
        });
        attackRateBtn.setOnMouseClicked(mouseEvent -> {
            if ((getSelectedTower().getAttackRate()/ getValAttackRate() < getMaxNumberOfEach())&&(getCostAttackRate() < Main.getUser().getMoney())&&(getSelectedTower()!=null)){
                Notification.showPopupMessage("Amélioration efféctuée !",Main.getStage());
                Main.getUser().addMoney(-getCostAttackRate());
                getSelectedTower().setAttackRate(getSelectedTower().getAttackRate()+ getValAttackRate());
                initialize(url,resourceBundle);
            }
            else {
                Notification.showPopupMessage("Vous ne pouvez effectuer cette amélioration",Main.getStage());
            }
        });


    }
}
