package Heroes.MenuInterface;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import Heroes.Exception.RessourceNotFoundException;
import Heroes.User.TowerImprovement;

import static Heroes.User.TowerImprovement.*;

/**
 * The type Tower improvement list view cell.
 */
public class TowerImprovementListViewCell extends ListCell<TowerImprovement> {
    /**
     * The Buy price label.
     */
    @FXML BorderPane bp ;
    /**
     * The Name label.
     */
    @FXML Label name;
    /**
     * The Damage label.
     */
    @FXML Label damage;
    /**
     * The Speed label.
     */
    @FXML Label speed ;
    /**
     * The Range label.
     */
    @FXML Label range ;
    /**
     * The Image label.
     */
    @FXML ImageView image;

    private FXMLLoader mLLoader;

    private void mlLoader() throws RessourceNotFoundException{
        try{
            mLLoader = new FXMLLoader(Main.getUser().getClass().getResource("/Heroes/MenuInterface/TowerImprovementListViewCell.fxml"));
            mLLoader.setController(this);
            mLLoader.load();
        }
        catch (Exception ex){
            throw new RessourceNotFoundException();
        }
    }

    /**
     * Initialize cell layout and attributes
     * @param tw a towerImprovement
     * @param empty boolean
     */
    protected void updateItem(TowerImprovement tw, boolean empty){
         super.updateItem(tw,empty);

        if(empty || tw == null){
            setText(null);
            setGraphic(null);
        }else{
            if(mLLoader == null){

                try {
                    mlLoader();
                } catch (RessourceNotFoundException e) {
                    System.out.println(e);
                }

                try {
                    image.setImage(new Image(tw.getTowerInfos().getAssetPath(),true));
                } catch (Exception e) {
                    image.setImage(null);
                    e.printStackTrace();
                }
            }
            // Set value to a cell
            name.setText(tw.getName());
            if ((tw.getAttack()+ getValAttack())/ getValAttack() <= getMaxNumberOfEach()){
                damage.setText("Attaque = " + String.valueOf(tw.getTowerInfos().getAttack()+tw.getAttack()) +", amélioration possible -> "+(tw.getTowerInfos().getAttack()+tw.getAttack()+ getValAttack()));
            }
            else damage.setText("Attaque = " + String.valueOf(tw.getTowerInfos().getAttack()+tw.getAttack()) +", amélioration imposssible.");

            if ((tw.getAttackRange()+ getValAttackRange())/ getValAttackRange() <= getMaxNumberOfEach()){
                range.setText("Portée attaque = " + String.valueOf(tw.getTowerInfos().getAttackRange()+tw.getAttackRange()) +", amélioration possible -> "+(tw.getTowerInfos().getAttackRange()+tw.getAttackRange()+ getValAttackRange()));
            }
            else range.setText("Portée attaque = " + String.valueOf(tw.getTowerInfos().getAttackRange()+tw.getAttackRange()) +", amélioration imposssible.");
            if ((tw.getAttackRate()+ getValAttackRate())/ getValAttackRate() <= getMaxNumberOfEach()){
                speed.setText("Vitesse d'attaque = " + String.valueOf(tw.getTowerInfos().getAttackRate()+tw.getAttackRate()) +", amélioration possible -> "+(tw.getTowerInfos().getAttackRate()+tw.getAttackRate()+ getValAttackRate()));
            }
            else speed.setText("Vitesse d'attaque = " + String.valueOf(tw.getTowerInfos().getAttack()+tw.getAttackRate()) +", amélioration imposssible.");

            setText(null);
            setGraphic(bp);
        }

    }
}
