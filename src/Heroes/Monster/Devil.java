package Heroes.Monster;

import Heroes.Music.Music;
import javafx.geometry.Rectangle2D;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

import java.io.File;
import java.util.ArrayList;

/**
 * The type Devil. (exteends Monster)
 * @author Jordan Rey-Jouanchicot
 */
public class Devil extends Monster {

	/**
	 * Instantiates a new Devil with default values and arguments and level
	 *
	 * @param path  the path
	 * @param root  the root
	 * @param xSh   the x sh
	 * @param ySh   the y sh
	 * @param level the level
	 */
	public Devil(ArrayList<Integer> path, Pane root, int xSh, int ySh, int level) {
		super(path, root,xSh, ySh,3);
		this.setMonsterKind(MonsterKinds.DEVIL);
		this.setLifePoints(4000+3800*level);
		this.setSpeed(3);
		this.setValue(3);
		this.setLevel(level);
		this.setDeath(false);
		this.render(root);
	}

	/**
	 * Render a devil
	 * @param root pane
	 */
	public void render(Pane root){
		// Show devil
		super.setImage( new Image("/assets/Monsters/devil.png"));
		root.getChildren().add(this);
	}

	/**
	 * Animate devil
	 * @param i  not used in actual version
	 */
	public void animate(int i){
		// Move in sprite sheet to be animated
		final int x = (getCount() % getColumns()) * 27;
		super.setViewport(new Rectangle2D(x, 0, 27, 60));
		super.setCount(super.getCount() + 1);
	}

	/**
	 * Play death sound for Devil
	 */
	public void deathSound() {
		// emit sound
		Music deathSound = new Music("devil.mp3");
		deathSound.deathSound();
	}
}
