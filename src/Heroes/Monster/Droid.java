package Heroes.Monster;

import Heroes.Music.Music;
import javafx.geometry.Rectangle2D;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;

import java.util.ArrayList;

/**
 * The type Droid (extends Monster)
 * @author Jordan Rey-Jouanchicot
 */
public class Droid extends Monster {

	/**
	 * Instantiates a new Droid.
	 *
	 * @param path  the path
	 * @param root  the root
	 * @param xSh   the x sh
	 * @param ySh   the y sh
	 * @param level the level
	 */
	public Droid(ArrayList<Integer> path, Pane root, int xSh, int ySh, int level) {
		super(path, root,xSh, ySh,6);
		this.setMonsterKind(MonsterKinds.DROID);
		this.setLifePoints(1500+1200*level);
		this.setSpeed(6);
		this.setValue(1);
		this.setLevel(level);
		this.setDeath(false);
		this.render(root);
	}

	/**
	 * Render Droid
	 * @param root pane
	 */
	public void render(Pane root){
		// Show image
		super.setImage( new Image("/assets/Monsters/droid.png"));
		root.getChildren().add(this);
	}

	/**
	 * Animate Droid
	 * @param i  not used in actual version
	 */
	public void animate(int i){
		// Move in spritesheet to be animated
		final int x = (getCount() % 2) * 42;
		super.setViewport(new Rectangle2D(x, 0, 42, 60));
		super.setCount(super.getCount() + 1);
	}
	/**
	 * Play death sound for Droid
	 */
	public void deathSound() {
		Music deathSound = new Music("droid.mp3");
		deathSound.deathSound();
	}

}
