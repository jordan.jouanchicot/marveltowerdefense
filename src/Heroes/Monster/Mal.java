package Heroes.Monster;

import Heroes.Music.Music;
import javafx.geometry.Rectangle2D;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;

import java.util.ArrayList;


/**
 * The type Mal (extends Monster)
 * @author Jordan Rey-Jouanchicot
 */
public class Mal extends Monster {


    /**
     * Instantiates a new Mal with default values and arguments and level
     *
     * @param path  the path
     * @param root  the root
     * @param xSh   the x sh
     * @param ySh   the y sh
     * @param level the level
     */
    public Mal(ArrayList<Integer> path, Pane root, int xSh, int ySh, int level) {
        super(path, root,xSh, ySh,7);
        this.setMonsterKind(MonsterKinds.MAL);
        this.setLifePoints(3000+2800*level);
        this.setSpeed(7);
        this.setValue(4);
        this.setLevel(level);
        this.setDeath(false);
        this.render(root);
    }

    /**
     * render Mal
     * @param root pane
     */
    public void render(Pane root){
        super.setImage( new Image("/assets/Monsters/mal.png"));
        root.getChildren().add(this);
    }

    /**
     * @param i not used in actual version
     */
    public void animate(int i){
        // Move in spritesheet to be animated
        final int x = (getCount() % 2) * 55;
        super.setViewport(new Rectangle2D(x, 0, 55, 50));
        super.setCount(super.getCount() + 1);
    }

    /**
     * Play death sound for Mal
     */
    public void deathSound() {
        Music deathSound = new Music("tour.mp3");
        deathSound.deathSound();
    }

}
