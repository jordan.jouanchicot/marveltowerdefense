package Heroes.Monster;

import javafx.scene.layout.Pane;

import java.util.ArrayList;

/**
 * The type Monster (extends MonsterRender)
 *
 * @author Jordan Rey-Jouanchicot
 */
public abstract class Monster extends MonsterRender {

	private MonsterKinds monsterKind;
	private int lifePoints;
	private int speed;
	private int value;
	private int level;
	private boolean death;

	/**
	 * Instantiates a new Monster.
	 *
	 * @param path  the path
	 * @param root  the root
	 * @param speed the speed
	 */
	public Monster(ArrayList<Integer> path, Pane root, int speed) {
		super(path, root, speed);
	}

	/**
	 * Instantiates a new Monster.
	 *
	 * @param path  the path
	 * @param root  the root
	 * @param xSh   the x sh
	 * @param ySh   the y sh
	 * @param speed the speed
	 */
	public Monster(ArrayList<Integer> path, Pane root, int xSh,int ySh,int speed) {
		super(path, root,xSh,ySh,speed);
	}

	/**
	 * Emit death sound
 	 */
	public abstract void deathSound();

	/**
	 * Gets speed.
	 *
	 * @return the speed
	 */
//Getter
	public int getSpeed() {
		return this.speed;
	}

	/**
	 * Gets monsterKind.
	 *
	 * @return the monsterKind
	 */
	public MonsterKinds getMonsterKind() {
		return monsterKind;
	}

	/**
	 * Sets monsterKind.
	 *
	 * @param monsterKind the monsterKind
	 */
	public void setMonsterKind(MonsterKinds monsterKind) {
		this.monsterKind = monsterKind;
	}

	/**
	 * Gets value.
	 *
	 * @return the value
	 */
	public int getValue() {
		return this.value;
	}

	/**
	 * Gets death.
	 *
	 * @return the death
	 */
	public boolean getDeath() {
		return this.death;
	}

	/**
	 * Sets speed.
	 *
	 * @param vi the speed
	 */
//Setter
	public void setSpeed(int vi) {
		this.speed = vi;
	}

	/**
	 * Sets value.
	 *
	 * @param g the value
	 */
	public void setValue(int g) {
		this.value = g;
	}

	/**
	 * Sets level.
	 *
	 * @param r the level
	 */
	public void setLevel(int r) {
		this.level = r;
	}

	/**
	 * Gets life points.
	 *
	 * @return the life points
	 */
//Methode
    public int getLifePoints() {
        return lifePoints;
    }

	/**
	 * Sets life points.
	 *
	 * @param lifePoints the life points
	 */
	public void setLifePoints(int lifePoints) {
        this.lifePoints = lifePoints;
    }


	/**
	 * Sets death.
	 *
	 * @param death the death
	 */
	public void setDeath(boolean death) {
		this.death = death;
	}
}
