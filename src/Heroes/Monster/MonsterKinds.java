package Heroes.Monster;

/**
 * The enum Monster kinds.
 * Avoid string comparison or class object switch case treatment
 */
public enum MonsterKinds {
    /**
     * Mal monster kinds.
     */
    MAL,
    /**
     * Devil monster kinds.
     */
    DEVIL,
    /**
     * Droid monster kinds.
     */
    DROID,
    /**
     * Titan monster kinds.
     */
    TITAN;


    private MonsterKinds() {}

}
