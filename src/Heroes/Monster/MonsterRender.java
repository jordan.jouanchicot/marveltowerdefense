package Heroes.Monster;


import Heroes.Tile.Tile;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import Heroes.Game;
import Heroes.Map.MapOfTiles;


import java.util.ArrayList;
import java.util.stream.IntStream;


/**
 * The type Monster render.
 * @author Jordan Rey-Jouanchicot
 */
public abstract class MonsterRender extends ImageView {
    // Atributes
    private ArrayList<Integer> xList = new ArrayList<Integer>();
    private ArrayList<Integer> yList = new ArrayList<>();
    private int xSh = 0;
    private int ySh = 0;

    private int count;
    private final int columns;
    private final int width;

    /**
     * Instantiates a new Monster render.
     *
     * @param path  the path
     * @param root  the root
     * @param speed the speed
     */
    public MonsterRender(ArrayList<Integer> path, Pane root, int speed){
        super();
        if (Tile.getTileSize() != Tile.getTileOriginalSize()) {
            super.setScaleX( (double) Tile.getTileSize() / Tile.getTileOriginalSize());
            super.setScaleY( (double) Tile.getTileSize() / Tile.getTileOriginalSize());
        }
        monsterMove(path);
        pathLinearize(path, 0,0,speed);
        this.count = 2;
        this.columns = 2;
        this.width = 40;
        render(root);
    }

    /**
     * Instantiates a new Monster render.
     *
     * @param path  the path
     * @param root  the root
     * @param xSh   the x sh
     * @param ySh   the y sh
     * @param speed the speed
     */
    public MonsterRender(ArrayList<Integer> path, Pane root,int xSh, int ySh, int speed){
        super();
        if (Tile.getTileSize() != Tile.getTileOriginalSize()) {
            super.setScaleX( (double) Tile.getTileSize() / Tile.getTileOriginalSize());
            super.setScaleY( (double) Tile.getTileSize() / Tile.getTileOriginalSize());
        }
        monsterMove(path,xSh,ySh);
        pathLinearize(path,xSh, ySh,speed);
        this.xSh =xSh;
        this.ySh = ySh;
        this.count = 2;
        this.columns = 2;
        this.width = 40;
    }

    /**
     * Render
     *
     * @param root the root
     */
    public abstract void render(Pane root);

    /**
     * Unrender.
     *
     * @param root the root
     */
    public void unrender(Pane root){
        root.getChildren().remove(this);
    }

    /**
     * Render.
     *
     * @param root the root
     * @param ress the ress
     */
    public void render(Pane root, String ress){
        super.setImage(new Image(ress));
        root.getChildren().add(this);
    }


    /**
     * Path linearize (linearize the path by getting a list of position to the monster to move to seem moving in a continuous)
     *
     * @param path  the path
     * @param xSh   the x shift of this monster
     * @param ySh   the y shift of this monster
     * @param speed the speed
     */
    public void pathLinearize(ArrayList<Integer> path,int xSh, int ySh, int speed) {
        int keyPrev = path.get(0);

        int xPrev = keyPrev % (Game.getWidth() / Tile.getTileSize());
        int yPrev = (keyPrev - xPrev) / (Game.getWidth() / Tile.getTileSize());

        int yPrevPix = -Game.getWidth() / 2 + (yPrev) * Tile.getTileSize() + MapOfTiles.getHeightShift() -ySh;
        int xPrevPix = -Game.getHeight() / 2 + (xPrev) * Tile.getTileSize()+ MapOfTiles.getWidthShift() -xSh;

        xList.add(xPrevPix);
        yList.add(yPrevPix);

        for (int i = 0; i<path.size(); i+=1){
            int key = path.get(i);
            int x = key % (Game.getWidth() / Tile.getTileSize());
            int y = (key - x) / (Game.getWidth() / Tile.getTileSize());
            int yPix = -Game.getWidth() / 2 + (y) * Tile.getTileSize() + MapOfTiles.getHeightShift();
            int xPix = -Game.getHeight() / 2 + (x) * Tile.getTileSize()+ MapOfTiles.getWidthShift();

            // Traitement des différents cas possible de positions relatives des deux cases
            if ((xPix-xPrevPix==0)&&(yPix>yPrevPix)){
                IntStream.rangeClosed(yPrevPix, yPix).filter(yT -> (yT%speed) ==0).forEach(yT ->{
                    xList.add(xPix);
                    yList.add(yT);
                });
            }
            else if ((xPrevPix-xPix==0)&&(yPix<yPrevPix)){
                int k = yList.size()-1;
                IntStream.rangeClosed(yPix, yPrevPix).filter(yT -> (yT%speed) ==0).forEach(yT->{
                    yList.add(k+1,yT);
                    xList.add(xPix);
                });
            }
            else if ((yPrevPix-yPix==0)&&(xPix>xPrevPix)){
                IntStream.rangeClosed(xPrevPix, xPix).filter(xT -> (xT%speed) ==0).forEach(xT->{
                    xList.add(xT);
                    yList.add(yPix);
                });
            }
            else if ((yPrevPix-yPix==0)&&(xPix<xPrevPix)){
                int k=xList.size()-1;
                IntStream.rangeClosed(xPix, xPrevPix).filter(xT -> (xT%speed) ==0).forEach(xT->{
                    xList.add(k+1,xT);
                    yList.add(yPix);
                });
            }
            else {
                IntStream.rangeClosed(xPrevPix,xPix).filter(yT -> (yT%speed) ==0).forEach(xT->{
                    xList.add(xT);
                    yList.add(yPix);
                });
            }

            xPrevPix = xPix;
            yPrevPix = yPix;
        }
    }


    /**
     * Path linearize with the actual path and the shifts (used to slowdown  by example)
     *
     * @param xListA the x list a
     * @param yListA the y list a
     * @param xSh    the x sh
     * @param ySh    the y sh
     * @param speed  the speed
     */
    public void pathLinearizeXY(ArrayList<Integer> xListA, ArrayList<Integer> yListA,int xSh, int ySh, int speed) {
        int yPrevPix = yListA.get(0);
        int xPrevPix = xListA.get(0);
        this.xList.add(xPrevPix);
        this.yList.add(yPrevPix);

        for (int i = 0; i<xListA.size(); i+=1){
            int yPix = yListA.get(i);
            int xPix = xListA.get(i);

            // Traitement
            if ((xPix-xPrevPix==0)&&(yPix>yPrevPix)){
                IntStream.rangeClosed(yPrevPix, yPix).filter(yT -> (yT%speed) ==0).forEach(yT ->{
                    xList.add(xPix);
                    yList.add(yT);
                });
            }
            else if ((xPrevPix-xPix==0)&&(yPix<yPrevPix)){
                int k = yList.size()-1;
                IntStream.rangeClosed(yPix, yPrevPix).filter(yT -> (yT%speed) ==0).forEach(yT->{
                    yList.add(k+1,yT);
                    xList.add(xPix);
                });
            }
            else if ((yPrevPix-yPix==0)&&(xPix>xPrevPix)){
                IntStream.rangeClosed(xPrevPix, xPix).filter(xT -> (xT%speed) ==0).forEach(xT->{
                    xList.add(xT);
                    yList.add(yPix);
                });
            }
            else if ((yPrevPix-yPix==0)&&(xPix<xPrevPix)){
                int k=xList.size()-1;
                IntStream.rangeClosed(xPix, xPrevPix).filter(xT -> (xT%speed) ==0).forEach(xT->{
                    xList.add(k+1,xT);
                    yList.add(yPix);
                });
            }
            else {
                IntStream.rangeClosed(xPrevPix,xPix).filter(yT -> (yT%speed) ==0).forEach(xT->{
                    xList.add(xT);
                    yList.add(yPix);
                });
            }

            xPrevPix = xPix;
            yPrevPix = yPix;
        }
    }

    /**
     * Monster move.
     *
     * @param path the path
     */
    public void monsterMove(ArrayList<Integer> path){
        // Calculate key as fist element of path
        int key = path.get(0);
        // calculate from key the position where the monster need to go and translate it there
        int x = key % (Game.getWidth() / Tile.getTileSize());
        int y = (key - x) / (Game.getWidth() / Tile.getTileSize());
        setTranslateX(-Game.getWidth() / 2 + x * Tile.getTileSize() + MapOfTiles.getHeightShift());
        setTranslateY(-Game.getHeight() / 2 + y * Tile.getTileSize() + MapOfTiles.getWidthShift());
        // put at foreground
        toFront();
    }


    /**
     * Monster move to next position
     *
     * @param path the path
     * @param xSh  the x sh
     * @param ySh  the y sh
     */
    public void monsterMove(ArrayList<Integer> path,int xSh, int ySh){
        // Calculate key as fist element of path
        int key = path.get(0);
        // calculate from key the position where the monster need to go and translate it there
        int x = key % (Game.getWidth() / Tile.getTileSize());
        int y = (key - x) / (Game.getWidth() / Tile.getTileSize());
        setTranslateX(-Game.getWidth() / 2 + x * Tile.getTileSize() + MapOfTiles.getHeightShift() -xSh);
        setTranslateY(-Game.getHeight() / 2 + y * Tile.getTileSize() + MapOfTiles.getWidthShift() -ySh);
        // put at foreground
        toFront();
    }

    /**
     * Monster move to next position
     *
     * @param xSh the x sh
     * @param ySh the y sh
     * @return the int
     */
    public int monsterMove(int xSh, int ySh) {
        // get first element of each list add some shift values and translate image to this locations
        setTranslateX(xList.get(0) - (240* Game.getWidth() /1200) - xSh-20);
        setTranslateY(yList.get(0) + (240* Game.getWidth() /1200) - ySh);
        // remove first element from x and y lists
        xList.remove(0);
        yList.remove(0);
        // Put monster at foreground
        toFront();
        return (xList.size());
    }

    /**
     * Gets size of xList.
     *
     * @return the size of list x
     */
    public int getSizeOfListX() {
        return xList.size();
    }

    /**
     * Gets size of yList.
     *
     * @return the size of list y
     */
    public int getSizeOfListY() {
        return yList.size();
    }

    /**
     * Animate.
     *
     * @param i the
     */
    public abstract void animate(int i);

    /**
     * Get xList.
     *
     * @return the array list
     */
    public ArrayList<Integer> getListX(){return xList;}

    /**
     * Get yList.
     *
     * @return the array list
     */
    public ArrayList<Integer> getListY(){return yList;}

    /**
     * Gets first element of xList.
     *
     * @return the list
     */
    public Integer getxList() {
        return xList.get(0);
    }

    /**
     * Gets first element of yList.
     *
     * @return the list
     */
    public Integer getyList() {
        return yList.get(0);
    }

    /**
     * Clear path lists
     */
    public void clearPath(){
        xList.clear();
        yList.clear();
    }

    /**
     * Gets xShift.
     *
     * @return the xShift
     */
    public int getxSh() {
        return xSh;
    }

    /**
     * Sets the xShift
     *
     * @param xSh the xShift
     */
    public void setxSh(int xSh) {
        this.xSh = xSh;
    }

    /**
     * Gets yShift.
     *
     * @return the yShift.
     */
    public int getySh() {
        return ySh;
    }

    /**
     * Sets yShift.
     *
     * @param ySh the yShift.
     */
    public void setySh(int ySh) {
        this.ySh = ySh;
    }


    /**
     * Gets count.
     *
     * @return the count
     */
    public int getCount() {
        return count;
    }

    /**
     * Sets count.
     *
     * @param count the count
     */
    public void setCount(int count) {
        this.count = count;
    }

    /**
     * Gets columns.
     *
     * @return the columns
     */
    public int getColumns() {
        return columns;
    }

    /**
     * Gets width.
     *
     * @return the width
     */
    public int getWidth() {
        return width;
    }
}
