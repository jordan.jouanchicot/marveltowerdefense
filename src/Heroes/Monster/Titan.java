package Heroes.Monster;

import Heroes.Music.Music;
import javafx.geometry.Rectangle2D;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;

import java.util.ArrayList;

/**
 * The type Titan (extends Monster)
 * @author Jordan Rey-Jouanchicot
 */
public class Titan extends Monster {


	/**
	 * Instantiates a new Titan with default values and arguments and level
	 *
	 * @param path  the path
	 * @param root  the root
	 * @param xSh   the x sh
	 * @param ySh   the y sh
	 * @param level the level
	 */
	public Titan(ArrayList<Integer> path, Pane root, int xSh, int ySh, int level) {
		super(path, root,xSh, ySh,2);
		this.setMonsterKind(MonsterKinds.TITAN);
		this.setLifePoints(12000+10000*level);
		this.setSpeed(2);
		this.setValue(10);
		this.setLevel(level);
		this.setDeath(false);
		this.render(root);
	}

	/**
	 * render Titan
	 * @param root pane
	 */
	public void render(Pane root){
		super.setImage( new Image("/assets/Monsters/titan.png"));
		root.getChildren().add(this);
	}

	/**
	 * animate Titan
	 * @param i notUsed in actual version
	 */
	public void animate(int i){
		// Move in spritesheet to be animated
		final int x = (getCount() % 3) * 40;
		super.setViewport(new Rectangle2D(x, 0, 40, 60));
		super.setCount(super.getCount() + 1);
	}
	/**
	 * Play death sound for Titan
	 */
	public void deathSound() {
		Music deathSound = new Music("mort.mp3");
		deathSound.deathSound();
	}


}
