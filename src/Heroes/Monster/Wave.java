package Heroes.Monster;

import javafx.scene.layout.Pane;
import Heroes.Tile.Tile;

import java.util.ArrayList;

/**
 * The type Wave.
 * @author Jordan Rey-Jouanchicot
 */
public class Wave {
    private ArrayList<Monster> monsters = new ArrayList<>();
    private int level;

    /**
     * Instantiates a new Wave.
     * @author Vincent
     * @param level the level
     * @param path  the path
     * @param root  the root
     */
    public Wave(int level, ArrayList<Integer> path, Pane root){
        // Create a copy of path
        ArrayList<Integer> pathT = new ArrayList<>(path);
        // generate waves with mathematics rules
        if (level%5==0) {
            monsters.add(new Titan(pathT,root,80,(4)%  (Tile.getTileSize() /2),level));
        }
        else if ((level+1)%5==0){
            for (int i =0 ; i < 3+0.8*level; i++) {
                monsters.add(new Droid(pathT, root, i*80,(-i*4)% (Tile.getTileSize() / 2),level));
            }

        }
        else if ((level+3)%5==0) {
            for (int i =0 ; i < 1+0.2*level; i++) {
                int val = (int) (Math.random()*2);
                if (val < 1){
                    monsters.add(new Mal(pathT,root,i*80,(-i*4)% (Tile.getTileSize() /2),level));
                }
                else if (val < 2) {
                    monsters.add(new Devil(pathT, root, i * 80, (-i * 4) % (Tile.getTileSize() / 2), level));
                }
            }
            for (int i =1 ; i < 0.5*level; i++) {
                monsters.add(new Droid(pathT, root, i*80,(-i*4)% (Tile.getTileSize() / 2),level));
            }
        }
        else {
            for (int i =0 ; i < 1+0.5*level; i++) {
                int val = (int) (Math.random()*2);
                if (val < 1){
                    monsters.add(new Mal(pathT,root,i*80,(-i*4)% (Tile.getTileSize() /2),level));
                }
                else if (val < 2) {
                    monsters.add(new Devil(pathT, root, i * 80, (-i * 4) % (Tile.getTileSize() / 2), level));
                }
            }

        }
        // Increase level
        this.level = level+1;
    }

    /**
     * Gets monsters(ArrayList of Monster)
     *
     * @return the monsters
     */
    public ArrayList<Monster> getMonsters() {
        return monsters;
    }

    /**
     * Gets level.
     *
     * @return the level
     */
    public int getLevel() {
        return level;
    }
}
