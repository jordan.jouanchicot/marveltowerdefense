package Heroes.Music;

import Heroes.Game;
import javafx.scene.media.*;
import javafx.util.Duration;

import java.io.File;

/*
 * Appelé le constructeur Music avec le nom du fichier en STRING sans le chemin ( déja implémenté )
 * POUR MUSIQUE EN CONTINUE ( Ambiance) : this.ambiance(); -> Pour l'arréter this.stop();
 * POUR UN BRUITAGE : this.sound();  ( bruit joué qu'une seul fois.
 */

/**
 * The type Music.
 * @author Etienne Tournier
 */
public class Music {
	private boolean isPlaying;
	private MediaPlayer player;



    /**
     * Instantiates a new Music.
     *
     * @param music the music
     */
    public Music(String music) {
        Media sound = new Media(new File("src/assets/music/"+music).toURI().toString());
        this.player= new MediaPlayer(sound);
        isPlaying = true;
    }

    /**
     * Play deathSound
     *
     */
    public void deathSound() {
        // if music is playing play deathsound
        if (Game.getMusic().isPlaying()){
            player.play();
        }
    }

    /**
     * Ambiance : play the music
     */
    public void ambiance() {
    	player.setAutoPlay(true);
    	isPlaying = true;
    	player.setOnReady(new Runnable() {
        	public void run() {
        		player.setStartTime(Duration.ZERO);
        		player.isAutoPlay();
        	}
        });
        
        player.setOnEndOfMedia(new Runnable() {
        	public void run() {
        		player.seek(Duration.ZERO);
        	}
        });
    }


    /**
     * Stop :  the music
     */
    public void stop() {
    	player.pause();
    	isPlaying = false;
    }

    /**
     * Is playing boolean.
     *
     * @return the boolean
     */
    public boolean isPlaying(){
        return isPlaying;
    }
    
    
}
