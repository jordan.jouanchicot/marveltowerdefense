package Heroes.Notification;

import javafx.animation.PauseTransition;
import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.stage.Popup;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.Duration;

/**
 * Notification class
 * @author Jordan Rey-Jouanchicot
 */
public class Notification {

    /**
     * Create a popup with the text of the string that close on click
     * @param message message (string) to show
     * @return popup
     */
    public static Popup createPopup(final String message) {
        // Create new popup and setup it
        final Popup popup = new Popup();
        popup.setAutoFix(true);
        popup.setAutoHide(true);
        popup.setHideOnEscape(true);
        Label label = new Label(message);
        label.setOnMouseReleased(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                popup.hide();
            }
        });
        label.getStylesheets().add("/Heroes/style.css");
        label.getStyleClass().add("popupT");
        popup.getContent().add(label);
        return popup;
    }

    /**
     * Show a popup
     * @param message text to set to the popup
     * @param stage stage used
     */
    public static void showPopupMessage(final String message, final Stage stage){
        // Create a popup with the message and setup it using createPopup
        final Popup popup = createPopup(message);
        // Define popup positiob
        popup.setOnShown(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent e) {
                popup.setX(stage.getX() + stage.getWidth()/2 - popup.getWidth()/2);
                popup.setY(stage.getY() +  stage.getHeight() - popup.getHeight());
            }
        });
        // Show popup
        popup.show(stage);
    }
}
