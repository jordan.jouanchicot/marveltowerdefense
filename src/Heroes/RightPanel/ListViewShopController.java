package Heroes.RightPanel;


import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import Heroes.Game;
import Heroes.Tile.TowerInfos;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * The type List view shop controller.
 * @author Nicolas
 */
public class ListViewShopController implements Initializable {

    /**
     * The List view.
     */
    @FXML  ListView<TowerInfos> listView = new ListView<TowerInfos>();
    private ObservableList<TowerInfos> towerlist ;
    /**
     * The Label.
     */
    @FXML Label label;

    /**
     * Instantiates a new List view shop controller.
     */
    public ListViewShopController(){
        towerlist = FXCollections.observableArrayList();
        // Get all towers that exist (all differents kinds) and add it to towerlist
        towerlist.addAll(Stream.of(TowerInfos.values()).collect(Collectors.toList()));
    }

    /**
     * Handle mouse click event :  selected a tower
     *
     * @param mouseEvent the mouse event
     */
    public void handleMouseClick(MouseEvent mouseEvent) {
        Game.setTowerselected(listView.getSelectionModel().getSelectedItem());
    }
    
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        listView.setItems(towerlist);
        label.setText("\n\n Liste des tours");
        // Initialize cells
        listView.setCellFactory(towerInfosListView -> new TowerListViewCell());
    }


}
