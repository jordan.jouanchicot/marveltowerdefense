package Heroes.RightPanel;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import Heroes.Exception.RessourceNotFoundException;
import Heroes.Game;
import Heroes.Tile.TowerInfos;

/**
 * The type Tower list view cell.
 * @author Nicolas
 */
public class TowerListViewCell extends ListCell<TowerInfos> {
    @FXML private BorderPane bp ;
    @FXML private Label name;
    @FXML private Label price ;
    @FXML private Label damage;
    @FXML private Label speed ;
    @FXML private Label range ;
    @FXML private Label specialEffect ;
    @FXML private ImageView image;
    @FXML private HBox hBox;
    private FXMLLoader mLLoader;


    private void mlLoader() throws RessourceNotFoundException{
        // Try to load cell template
        try{
            mLLoader = new FXMLLoader(getClass().getResource("/Heroes/RightPanel/ListViewCell.fxml"));
            mLLoader.setController(this);
            mLLoader.load();
        }
        catch (Exception ex){
            throw new RessourceNotFoundException();
        }
    }

    /**
     * Initialize cell layout and attributes
     * @param towerInfos infos of a tower
     * @param empty empty
     */
    protected void updateItem(TowerInfos towerInfos, boolean empty){
         super.updateItem(towerInfos,empty);

        if(empty || towerInfos == null){
            setText(null);
            setGraphic(null);
        }else{
            if(mLLoader == null){
                try {
                    mlLoader();
                } catch (RessourceNotFoundException e) {
                    System.out.println(e);
                }
            }

            // Try to show image of tower
            try {
                Image ironman = new Image(towerInfos.getAssetPath(),true);
                image.setImage(ironman);
            } catch (Exception e) {
                image.setImage(null);
            }
            // set infos to tower
            name.setText(towerInfos.getName());
            price.setText("Prix = " + String.valueOf(towerInfos.getPrice()));
            damage.setText("Attaque = "+ (Game.getUser().getTowersImprovements().towerImprovements(towerInfos).getAttack()+towerInfos.getAttack()));
            speed.setText("Vitesse = " + (Game.getUser().getTowersImprovements().towerImprovements(towerInfos).getAttackRate()+towerInfos.getAttackRate()));
            range.setText("Range = " + (Game.getUser().getTowersImprovements().towerImprovements(towerInfos).getAttackRange()+towerInfos.getAttackRange()));
            if (towerInfos.getSpecialEffect() != null){
                specialEffect.setText("Effet spécial : "+ towerInfos.getSpecialEffect());
            }
            else {
                specialEffect.setText("Effet spécial : Elle n'en a pas besoin");
            }
            setText(null);
            setGraphic(bp);
        }

    }
}
