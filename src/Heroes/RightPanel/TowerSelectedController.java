package Heroes.RightPanel;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import Heroes.Game;
import Heroes.Map.MapOfTiles;
import Heroes.Tile.TileKinds;
import Heroes.Tile.TowerStates;
import Heroes.Tile.Towers.Tower;


import java.net.URL;
import java.util.ResourceBundle;

/**
 * The type Tower selected controller, show the infos of a Tower and show the choices possible.
 * @author Nicolas
 */
public class TowerSelectedController implements Initializable{
    @FXML private Label label0;
    @FXML private Label label1;
    @FXML private Label label2;
    @FXML private Label label3;
    @FXML private ImageView imageV;
    @FXML private Button upgrade;
    @FXML private Button sell;

    /**
     * Instantiates a new Tower selected controller.
     */
    public TowerSelectedController(){ }

    /**
     * Improve a tower if possible (used on clik)
     * @param map map of tiles
     * @param root Pane
     */
    public void improveTower(MapOfTiles map, Pane root){
        if (Game.getInGameUser().getMoney()>= Game.getLt().get(Game.getMouse().getTileKey()).getBuyPrice()) {
            map.unrender(Game.getMouse().getTileKey(), root);
            Game.getInGameUser().takeMoney(Game.getLt().get(Game.getMouse().getTileKey()).getBuyPrice());
            Game.getLt().put(Game.getMouse().getTileKey(), Game.getLt().get(Game.getMouse().getTileKey()).incLevel(root));
        }
    }

    /**
     * Remove tower from map
     *
     * @param map  the map
     * @param root the root
     */
    public void removeTower(MapOfTiles map,Pane root){
        map.unrender(Game.getMouse().getTileKey(),root);
        Game.getMap().getTiles().get(Game.getMouse().getTileKey()).setTileKind(TileKinds.CONSTRUCTIBLE);
        Game.getInGameUser().addMoney(Game.getLt().get(Game.getMouse().getTileKey()).getSellPrice());
        Game.getLt().remove(Game.getMouse().getTileKey());
        Game.getPane().setTop(Game.getPane1());
        Game.getPane().setRight(Game.getPane3());

    }


    /**
     * Button upgrade a tower
     *
     * @param mouseEvent the mouse event
     */
    public void btupgrade(MouseEvent mouseEvent) {
        improveTower(Game.getMap(), Game.getRoot());
        Game.getPane().setRight(Game.getPane3());
    }

    /**
     * Button sell action (sell a tower)
     *
     * @param mouseEvent the mouse event
     */
    public void btsell(MouseEvent mouseEvent){
        removeTower(Game.getMap(), Game.getRoot());
        Game.getPane().setRight(Game.getPane3());
    }

    /**
     * Button back : go back to the view of list to buy
     *
     * @param mouseEvent the mouse event
     */
    public void btback(MouseEvent mouseEvent){
        Game.getPane().setRight(Game.getPane3());
    }


    /**
     * get all the infos and show infos about a tower in a pane
     * @param url url
     * @param resourceBundle ressourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        // get all the infos and show infos about a tower
        Tower tower = Game.getLt().get(Game.getMouse().getTileKey());
        try {
            imageV.setImage(tower.getImage());
        }
        catch (Exception e){
            imageV.setImage(null);
            System.out.println("Error with Java and your OS for this operation.");
        }
        label0.setText("\n\n\nVous avez séléctionné une tour "+tower.getName()  +" " + tower.getState() );
        label1.setText("Attaque =  "+ String.valueOf(tower.getDamage()));
        label2.setText("Vitesse attaque = " +String.valueOf(tower.getAttackSpeed()));
        label3.setText("Portée attaque = " +String.valueOf(tower.getRange()));
        sell.setText("Vendre pour "+String.valueOf(tower.getSellPrice()));
        if (tower.getState() == TowerStates.LEVEL3){
            upgrade.setText("Vous ne pouvez améliorer cette tour");
        }
        else upgrade.setText("Améliorer pour "+String.valueOf(tower.getBuyPrice()));
        // imageV.setImage(image);
    }
}
