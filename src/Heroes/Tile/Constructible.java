package Heroes.Tile;

/**
 * The type Constructible (extends Tile)
 * @author Jordan Rey-Jouanchicot
 */
public class Constructible extends Tile {
    private TowerStates state;

    // ATTIBUTE OF ANY TOWER
    private int attack;
    private int attackRate;
    private int attackRange;

    /**
     * Instantiates a new Constructible.
     *
     * @param x           the x Position
     * @param y           the y position
     * @param tileKind    the tile kind
     * @param state       the state
     * @param attack      the attack
     * @param attackRate  the attack rate
     * @param attackRange the attack range
     */
    public Constructible(int x, int y, TileKinds tileKind, TowerStates state, int attack, int attackRate, int attackRange) {
        super(x, y-(double) 1/3 , tileKind);
        this.state = state;
        this.attack = attack;
        this.attackRate = attackRate;
        this.attackRange = attackRange;
    }

    /**
     * Gets state.
     *
     * @return the state
     */
    public TowerStates getState() {
        return state;
    }

    /**
     * Gets attack.
     *
     * @return the attack
     */
    public int getAttack() {
        return attack;
    }

    /**
     * Gets attack rate.
     *
     * @return the attack rate
     */
    public int getAttackRate() {
        return attackRate;
    }

    /**
     * Gets attack range.
     *
     * @return the attack range
     */
    public int getAttackRange() {
        return attackRange;
    }

    /**
     * Sets state.
     *
     * @param state the state
     */
    protected void setState(TowerStates state) {
        this.state = state;
    }

    /**
     * Sets attack.
     *
     * @param attack the attack
     */
    protected void setAttack(int attack) {
        this.attack = attack;
    }

    /**
     * Sets attack rate.
     *
     * @param attackRate the attack rate
     */
    protected void setAttackRate(int attackRate) {
        this.attackRate = attackRate;
    }

    /**
     * Sets attack range.
     *
     * @param attackRange the attack range
     */
    protected void setAttackRange(int attackRange) {
        this.attackRange = attackRange;
    }
}
