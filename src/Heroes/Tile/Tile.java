package Heroes.Tile;

import Heroes.Game;
import Heroes.Map.MapOfTiles;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;

/**
 * The type Tile.
 * @author Jordan Rey-Jouanchicot
 */
public class Tile extends ImageView {

    private static int tileOriginalSize =  40;
    private static int tileSize =  60;
    private static int ran = (int) (Math.random()*2);
    
    private TileKinds tileKinds;

    /**
     * Instantiates a new Tile.
     */
    public Tile() {
        tileKinds = TileKinds.NOT_CONSTRUCTIBLE;
    }

    /**
     * Instantiates a new Tile.
     * Translate it to the coordinates given
     * @param x        the x position
     * @param y        the y position
     * @param tileKind the tile kind
     */
    public Tile(int x, double y, TileKinds tileKind) {
        super();
        // tile scaling
        if (tileSize != tileOriginalSize) {
            super.setScaleX( (double) tileSize/tileOriginalSize);
            super.setScaleY( (double) tileSize/tileOriginalSize);
        }
        // tile translation to position in parameter
        setTranslateX(-Game.getWidth() / 2 + x * Tile.tileSize + MapOfTiles.getHeightShift());
        setTranslateY( -Game.getHeight() / 2 + (y) * Tile.tileSize+ MapOfTiles.getWidthShift());
        // set tilekind of tile
        tileKinds = tileKind;
    }

    /**
     * Instantiates a new Tile  only used for not constructibme TILE
     * Translate it to the coordinates given
     * @param x the x
     * @param y the y
     */
    public Tile(int x, int y) {
        super();
        // tile scaling
        if (tileSize != tileOriginalSize) {
            super.setScaleX( (double) tileSize/tileOriginalSize);
            super.setScaleY( (double) tileSize/tileOriginalSize);
        }
        // tile translation to position in parameter
        setTranslateX(-Game.getWidth() / 2 + x * Tile.tileSize + MapOfTiles.getHeightShift());
        setTranslateY( -Game.getHeight() / 2 + (y) * Tile.tileSize+ MapOfTiles.getWidthShift());
        // set tilekind to not_constructible
        tileKinds = TileKinds.NOT_CONSTRUCTIBLE;
    }

    /**
     * Get tile kind tile kinds.
     *
     * @return the tile kinds
     */
    public TileKinds getTileKind(){
        return tileKinds;
    }

    /**
     * Set tile kind.
     * @param tk the tilekind
     */
    public void setTileKind(TileKinds tk){
        tileKinds = tk;
    }

    /**
     * Render not constructible and constructibles tiles
     * Doesn't render path
     * @param root the root
     * @author Vincent and Jordan
     */
    public void render(Pane root){
        // If texture pack 1
        if (ran < 1) {
            if (tileKinds == TileKinds.NOT_CONSTRUCTIBLE) {
                int rand = (int) (Math.random() * 20);
                if (rand < 12) {
                    super.setImage(new Image("/assets/map/tree.png"));
                } else if (rand < 17) {
                    super.setImage(new Image("/assets/map/rock.png"));
                } else if (rand < 19) {
                    super.setImage(new Image("/assets/map/house.png"));
                } else if (rand < 20) {
                    super.setImage(new Image("/assets/map/well.png"));
                }
                root.getChildren().add(this);
            } else if (tileKinds == TileKinds.CONSTRUCTIBLE) {
                super.setImage(new Image("/assets/map/constructible.png"));
                root.getChildren().add(this);
            }
        }
        else {
            // If texture pack 2
            if (tileKinds == TileKinds.NOT_CONSTRUCTIBLE) {
                int rand = (int) (Math.random() * 200);
                if (rand < 70) {
                    super.setImage(new Image("/assets/map2/house1.png"));
                } else if (rand < 120) {
                    super.setImage(new Image("/assets/map2/house2.png"));
                } else if (rand < 185) {
                    super.setImage(new Image("/assets/map2/building.png"));
                } else if (rand < 196) {
                    super.setImage(new Image("/assets/map2/shop.png"));
                } else if (rand < 199) {
                    super.setImage(new Image("/assets/map2/school.png"));
                } else if (rand < 200) {
                    super.setImage(new Image("/assets/map2/church.png"));
                }
                root.getChildren().add(this);
            } else if (tileKinds == TileKinds.CONSTRUCTIBLE) {
                super.setImage(new Image("/assets/map2/constructi.png"));
                root.getChildren().add(this);
            }
        }
    }

    /**
     * Render
     *
     * @param root the root
     * @param ress the ressource to set (image)
     */
    public void render(Pane root, String ress){
        super.setImage(new Image(ress));
        root.getChildren().add(this);
    }

    /**
     * Unrender.
     *
     * @param root the root
     */
    public void unrender(Pane root){
        root.getChildren().remove(this);
    }


    /**
     * Gets tile original size.
     *
     * @return the tile original size
     */
    public static int getTileOriginalSize() {
        return tileOriginalSize;
    }

    /**
     * Gets tile size.
     *
     * @return the tile size
     */
    public static int getTileSize() {
        return tileSize;
    }

    /**
     * Sets tile size.
     *
     * @param tileSize the tile size
     */
    public static void setTileSize(int tileSize) {
        Tile.tileSize = tileSize;
    }

    /**
     * Gets random value of map.
     *
     * @return the random value
     */
    public static int getRan() {
        return ran;
    }
}
