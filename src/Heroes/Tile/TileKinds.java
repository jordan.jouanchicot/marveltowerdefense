package Heroes.Tile;

/**
 * The enum Tile kinds.
 * Avoid string comparison or class object switch case treatment
 */
public enum TileKinds {
    /**
     * Constructible tile kinds.
     */
    CONSTRUCTIBLE,
    /**
     * Not constructible tile kinds.
     */
    NOT_CONSTRUCTIBLE,
    /**
     * Path tile kinds.
     */
    PATH,
    /**
     * Greenlizard tile kinds.
     */
    GREENLIZARD,
    /**
     * Lazerman tile kinds.
     */
    LAZERMAN,
    /**
     * Captain tile kinds.
     */
    CAPTAIN,
    /**
     * God tile kinds.
     */
    GOD,
    /**
     * Ultralady tile kinds.
     */
    ULTRALADY;



    private TileKinds() { }

}
