package Heroes.Tile;

/**
 * The enum Tower infos.  (all stats of towers)
 */
public enum TowerInfos {
    /**
     * The Greenlizard.
     */
    GREENLIZARD(300,"Green Lizard",600,40,3*Tile.getTileSize(),"/assets/towers/ToSpi1D.png","Peut ralentir l'enemi"),
    /**
     * The Lazerman, Kylor.
     */
    LAZERMAN(500,"Kylor",1000,40,4*Tile.getTileSize(),"/assets/towers/ToIron1D.png","Peut attaquer plus loin et dégats x2"),
    /**
     * The Captain, Wildclaw.
     */
    CAPTAIN(600, "Wildclaw", 1300,30,3*Tile.getTileSize(),"/assets/towers/ToCapt1D.png", "Peut repousser l'enemi"),
    /**
     * The God, Ragnar.
     */
    GOD(1000, "Ragnar", 2000, 10, 5*Tile.getTileSize(), "/assets/towers/ToThor1D.png","Peut attaquer plusieurs enemis et plus loin"),
    /**
     * The Ultralady, Felinya.
     */
    ULTRALADY(400, "Felinya", 400,100,2*Tile.getTileSize(),"/assets/towers/ToBW1D.png","Peut avoir ses dégats x2 et portée x1.5");
    private final int buyPrice;
    private final String name;
    private final int attack;
    private final int attackRate;
    private final int attackRange;
    private final String assetPath;
    private final String specialEffect;

    private TowerInfos(int bp, String name, int attack, int attackRate, int attackRange, String assetPath, String specialEffect) {
        this.name = name;
        this.buyPrice = bp;
        this.attack = attack;
        this.attackRange = attackRange;
        this.attackRate = attackRate;
        this.assetPath = assetPath;
        this.specialEffect = specialEffect;
    }

    /**
     * Gets price.
     *
     * @return the price
     */
    public int getPrice() {
        return buyPrice;
    }

    /**
     * Get name string.
     *
     * @return the name
     */
    public String getName(){
        return name;
    }

    /**
     * Gets attack.
     *
     * @return the attack
     */
    public int getAttack() {
        return attack;
    }

    /**
     * Gets attack rate.
     *
     * @return the attack rate
     */
    public int getAttackRate() {
        return attackRate;
    }

    /**
     * Gets attack range.
     *
     * @return the attack range
     */
    public int getAttackRange() {
        return attackRange;
    }

    /**
     * Get asset path string.
     *
     * @return the path to source (asset)
     */
    public String getAssetPath(){
        return assetPath;
    }

    /**
     * Get special effect string.
     *
     * @return the special effect explained
     */
    public String getSpecialEffect(){
        return specialEffect;
    }
}
