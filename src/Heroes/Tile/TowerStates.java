package Heroes.Tile;

/**
 * The enum Tower states.
 */
public enum TowerStates {
    /**
     * Empty tower states.
     */
    EMPTY(0),
    /**
     * Level 1 tower states.
     */
    LEVEL1(1),
    /**
     * Level 2 tower states.
     */
    LEVEL2(2),
    /**
     * Level 3 tower states.
     */
    LEVEL3(3);
    private final int value;

    private TowerStates(int value) {
        this.value = value;
    }

    /**
     * Gets tower state.
     *
     * @return the tower state
     */
    public int getTowerState() {
        return value;
    }

    /**
     * Inc tower states.
     *
     * @return the tower states
     */
    public TowerStates inc() {
        System.out.println("State :"+this.getTowerState());
        if (getTowerState() == 1){
            return TowerStates.LEVEL2 ;
        }
        else if (getTowerState() == 2){
            return TowerStates.LEVEL3 ;
        }
        else return this;
    }
}
