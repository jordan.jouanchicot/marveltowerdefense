package Heroes.Tile.Towers.Attacks;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;

import java.util.ArrayList;

/**
 * The type Attack (extends attackRender)
 * @author Nicolas Mah-Chak
 */
public abstract class Attack extends AttackRender {
    // Attributes
    private String name;
    private int speed;
    private String source;

    /**
     * Instantiates a new Attack.
     *
     * @param towerKey the tower key
     * @param xPix     the x pix
     * @param yPix     the y pix
     * @param root     the root
     * @param speed    the speed
     */
    public Attack(int towerKey,int xPix,int yPix, Pane root, int speed) {
        super(towerKey,xPix,yPix,root,speed);
    }


    /**
     * Instantiates a new Attack.
     *
     * @param towerKey the tower key
     * @param xPix     the x pix
     * @param yPix     the y pix
     * @param speed    the speed
     */
    public Attack(int towerKey,int xPix,int yPix, int speed) {
        super(towerKey,xPix,yPix,speed);
    }

    /**
     * Instantiates a new Attack.
     *
     * @param xPix           the x pix
     * @param yPix           the y pix
     * @param root           the root
     * @param speed          the speed
     * @param numberOfFrames the number of frames
     */
    public Attack(int xPix,int yPix, Pane root, int speed, int numberOfFrames) {
        super(xPix,yPix,root,speed, numberOfFrames);
    }

    /**
     * Get speed int value
     *
     * @return the speed
     */
    //Getter
    public int getSpeed(){ return speed;}

    /**
     * Get source string of an attack sprite
     *
     * @return the string
     */
    public String getSource(){return  this.source;}

    /**
     * Set speed
     *
     * @param x the speed
     */
    //Setter
    public void setSpeed(int x){speed =x; }


    /**
     * Sets source of an attack sprite
     *
     * @param source the source
     */
    public void setSource(String source) {
        this.source = source;
    }

    /**
     * Gets name of an attack
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name of an attack
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }
}
