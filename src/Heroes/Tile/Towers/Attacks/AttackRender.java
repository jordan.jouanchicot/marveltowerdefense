package Heroes.Tile.Towers.Attacks;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import Heroes.Game;
import Heroes.Map.MapOfTiles;
import Heroes.Tile.Tile;
import java.util.ArrayList;

/**
 * The type Attack render.
 * @author Nicolas Mah-Chak
 */
public class AttackRender extends ImageView {
    // Attributes
    private ArrayList<Integer> DxList = new ArrayList<>();
    private ArrayList<Integer> DyList = new ArrayList<>();
    private int towerx;
    private int towery;
    private int monsterx;
    private int monstery;

    /**
     * Instantiates a new Attack render.
     *
     * @param towerKey the tower key
     * @param xPix     the x pix
     * @param yPix     the y pix
     * @param root     the root
     * @param speed    the speed
     */
    public AttackRender(int towerKey,int xPix,int yPix, Pane root, int speed) {
        super();
        // Linearize path (make points inside) to make animation smooth
        pathLinearize2(towerKey,xPix,yPix,0,0,speed);
        // Define monster xPosition and monster yPosition
        this.monsterx = xPix;
        this.monstery = yPix;
        // used for rotate
        int nbCaseHaut= Game.getHeight()/Tile.getTileSize();
        int x= towerKey % (Game.getWidth() / Tile.getTileSize());
        towerx = (towerKey % (Game.getWidth() / Tile.getTileSize())+1)*Tile.getTileSize()-(int)(Tile.getTileSize()*5.5);
        towery = -((-((towerKey - x) / (Game.getWidth() / Tile.getTileSize()) - (nbCaseHaut-1)) * Tile.getTileSize()) - Tile.getTileSize() / 2);
        // rotate attack image
        rotate();
    }

    /**
     * Instantiates a new Attack render for shield
     *
     * @param towerKey the tower key
     * @param xPix     the x pix
     * @param yPix     the y pix
     * @param speed    the speed
     */
    public AttackRender(int towerKey,int xPix,int yPix, int speed) {
        super();
        // Linearize path (make points inside) to make animation smooth and make the same in the opposite way (make the attack come back)
        pathLinearizeShield(towerKey,xPix,yPix,0,0,speed);
        // Define monster xPosition and monster yPosition
        this.monsterx = xPix;
        this.monstery = yPix;
        // used for rotate
        int nbCaseHaut= Game.getHeight()/Tile.getTileSize();
        int x= towerKey % (Game.getWidth() / Tile.getTileSize());
        towerx = (towerKey % (Game.getWidth() / Tile.getTileSize())+1)*Tile.getTileSize()-(int)(Tile.getTileSize()*5.5);
        towery = -((-((towerKey - x) / (Game.getWidth() / Tile.getTileSize()) - (nbCaseHaut-1)) * Tile.getTileSize()) - Tile.getTileSize() / 2);
        // rotate attack image
        rotate();
    }


    /**
     * Instantiates a new Attack render.
     *
     * @param xPix           the x pix
     * @param yPix           the y pix
     * @param root           the root
     * @param speed          the speed
     * @param numberOfFrames the number of frames
     */
    public AttackRender(int xPix,int yPix, Pane root, int speed, int numberOfFrames) {
        super();
        for (int i=yPix-numberOfFrames*speed; i <= yPix;i+=speed){
            DxList.add(xPix);
            DyList.add(i);
            rotate();
        }
    }
    /**
     * Rotate the attack image to fit with the attack direction
     */
    public void rotate() {
        // Calculate angle of rotation
        double angle =-Math.atan2(((-monstery+Tile.getTileSize()) - (-towery+Tile.getTileSize())),((monsterx+Tile.getTileSize()*5) - (towerx+Tile.getTileSize()*5)))* 180 / Math.PI;
        // rotate attack image
        setRotate(angle);
    }

    /**
     * Path linearize with key of tower and predicted intesect point for an attack animation and the monster
     *
     * @param towerKey the tower key
     * @param xPix     the x pix
     * @param yPix     the y pix
     * @param xSh      the x sh
     * @param ySh      the y sh
     * @param speed    the speed
     */
    public void pathLinearize2(int towerKey, int xPix, int yPix,int xSh, int ySh, int speed) {
        // calculate x and y of tower in Pixels from key
        int keyPrev = towerKey;

        int xPrev = keyPrev % (Game.getWidth() / Tile.getTileSize());
        int yPrev = (keyPrev - xPrev) / (Game.getWidth() / Tile.getTileSize());

        int towery = -Game.getWidth() / 2 + (yPrev) * Tile.getTileSize() + MapOfTiles.getHeightShift() - ySh;
        int towerx = -Game.getHeight() / 2 + (xPrev) * Tile.getTileSize() + MapOfTiles.getWidthShift() - xSh;

        // Add the calculate position as original positions
        DxList.add(towerx);
        DyList.add(towery);
        int xdiff = towerx - xPix;
        if((xdiff >= 0 && xPix<=0 && towerx<=0) ) xdiff *=-1;
        else if(xdiff <= 0 && xPix <=0 && towerx<=0) xdiff *=-1;
        else if(xdiff >= 0 && xPix>=0 && towerx>=0) xdiff *=-1;
        else if(xdiff <= 0 && xPix <=0 && towerx>=0) xdiff *=-1;
        else if(xdiff <= 0 && xPix >=0 && towerx<=0) xdiff *=-1;
        else if(xdiff >= 0 && xPix <=0 && towerx>=0) xdiff *=-1;
        else if(xdiff <= 0 && xPix >=0 && towerx>=0) xdiff *=-1;

        int ydiff = towery - yPix;
        if(ydiff >= 0 && yPix <= 0 && towery<=0) ydiff *=-1;
        else if(ydiff <= 0 && yPix <= 0 && towery<=0) ydiff *=-1;
        else if(ydiff >= 0 && yPix >= 0 && towery>=0) ydiff *=-1;
        else if(ydiff <= 0 && yPix <= 0 && towery>=0) ydiff *=-1;
        else if(ydiff <= 0 && yPix >= 0 && towery<=0) ydiff *=-1;
        else if(ydiff >= 0 && yPix <= 0 && towery<=0) ydiff *=-1;
        else if(ydiff <= 0 && yPix >= 0 && towery<=0) ydiff *=-1;

        // Define distance between each points
        int xincr = xdiff/speed;
        int yincr = ydiff/speed;

        // Add points to the lists of positions of attack
        for(int i = 1; i<speed;i++){
            DxList.add(towerx+(xincr*i));
            DyList.add(towery+(yincr*i));
        }
        DxList.add(xPix);
        DyList.add(yPix);
    }

    public void pathLinearizeInv(int towerKey, int xPix, int yPix,int xSh, int ySh, int speed) {
        // calculate x and y of tower in Pixels from key
        int size = getDListX().size();
        int keyPrev = towerKey;

        int xPrev = keyPrev % (Game.getWidth() / Tile.getTileSize());
        int yPrev = (keyPrev - xPrev) / (Game.getWidth() / Tile.getTileSize());

        int towery = -Game.getWidth() / 2 + (yPrev) * Tile.getTileSize() + MapOfTiles.getHeightShift() - ySh;
        int towerx = -Game.getHeight() / 2 + (xPrev) * Tile.getTileSize() + MapOfTiles.getWidthShift() - xSh;

        // Add the calculate position as original positions
        DxList.add(size,towerx);
        DyList.add(size,towery);

        int xdiff = towerx - xPix;
        if((xdiff >= 0 && xPix<=0 && towerx<=0) ) xdiff *=-1;
        else if(xdiff <= 0 && xPix <=0 && towerx<=0) xdiff *=-1;
        else if(xdiff >= 0 && xPix>=0 && towerx>=0) xdiff *=-1;
        else if(xdiff <= 0 && xPix <=0 && towerx>=0) xdiff *=-1;
        else if(xdiff <= 0 && xPix >=0 && towerx<=0) xdiff *=-1;
        else if(xdiff >= 0 && xPix <=0 && towerx>=0) xdiff *=-1;
        else if(xdiff <= 0 && xPix >=0 && towerx>=0) xdiff *=-1;

        int ydiff = towery - yPix;
        if(ydiff >= 0 && yPix <= 0 && towery<=0) ydiff *=-1;
        else if(ydiff <= 0 && yPix <= 0 && towery<=0) ydiff *=-1;
        else if(ydiff >= 0 && yPix >= 0 && towery>=0) ydiff *=-1;
        else if(ydiff <= 0 && yPix <= 0 && towery>=0) ydiff *=-1;
        else if(ydiff <= 0 && yPix >= 0 && towery<=0) ydiff *=-1;
        else if(ydiff >= 0 && yPix <= 0 && towery<=0) ydiff *=-1;
        else if(ydiff <= 0 && yPix >= 0 && towery<=0) ydiff *=-1;

        // Define distance between each points
        int xincr = xdiff/speed;
        int yincr = ydiff/speed;

        // Add points to the lists of positions of attack
        for(int i = 1; i<speed;i++){
            DxList.add(size,towerx+(xincr*i));
            DyList.add(size,towery+(yincr*i));
        }
        DxList.add(size,xPix);
        DyList.add(size,yPix);
    }


    /**
     * Path linearize with key of tower and predicted intesect point for an attack animation and the monster
     *
     * @param towerKey the tower key
     * @param xPix     the x pix
     * @param yPix     the y pix
     * @param xSh      the x sh
     * @param ySh      the y sh
     * @param speed    the speed
     */
    public void pathLinearizeShield(int towerKey, int xPix, int yPix,int xSh, int ySh, int speed) {
        pathLinearize2(towerKey, xPix, yPix, xSh, ySh, speed);
        pathLinearizeInv(towerKey, xPix, yPix, xSh, ySh, speed);
    }

    /**
     * Unrender attack
     *
     * @param root the root
     */
    public void unrender(Pane root){
        root.getChildren().remove(this);
    }

    /**
     * Render attack
     *
     * @param root the root
     * @param ress the ress
     */
    public void render(Pane root, String ress){
        // set image attack to ress
        super.setImage(new Image(ress));
        // Translate attack to its first position
        setTranslateX(DxList.get(0)- (240* Game.getWidth() /1200)  - 20);
        setTranslateY(DyList.get(0) + (240* Game.getWidth() /1200) );
        // add to root
        root.getChildren().add(this);
    }

    /**
     * Attack move to next x et y and delete them from paths lists
     */
    public void attackMove() {
        // get first element of each list add some shift values and translate image to this locations
        setTranslateX(DxList.get(0)- (240* Game.getWidth() /1200)  - 20);
        setTranslateY(DyList.get(0) + (240* Game.getWidth() /1200) );
        // remove first element of each list
        DxList.remove(0);
        DyList.remove(0);
    }

    /**
     * Get DListX
     *
     * @return the array list
     */
    public ArrayList<Integer> getDListX(){return DxList;}
}
