package Heroes.Tile.Towers.Attacks;

import javafx.scene.image.Image;
import javafx.scene.layout.Pane;

import java.util.ArrayList;


/**
 * The type Laser (extends Attack)
 */
public class Laser extends Attack {

    /**
     * Instantiates a new Laser.
     *
     * @param towerKey the tower key
     * @param xPix     the xPredicted monster position in speed (speed =number of frame)
     * @param yPix     the yPredicted monster position in speed (speed =number of frame)
     * @param root     the root
     * @param speed    the speed
     */
    public Laser(int towerKey, int xPix, int yPix, Pane root, int speed) {
        super(towerKey,xPix,yPix,root,speed);
        this.setName("laser");
        this.setSpeed(speed);
        this.setSource("assets/attacks/laser.png");
    }

    /**
     * Render Laser)
     *
     * @param root the root
     */
    public void render(Pane root) {
        super.setImage(new Image(getSource()));
        root.getChildren().add(this);
    }
}
