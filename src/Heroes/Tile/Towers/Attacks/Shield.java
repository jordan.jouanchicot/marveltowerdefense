package Heroes.Tile.Towers.Attacks;

import javafx.scene.layout.Pane;

/**
 * The type Shield (extends Attack)
 * @author Nicolas Mah-Chak
 */
public class Shield extends Attack {

    /**
     * Instantiates a new Shield.
     *
     * @param towerKey the tower key
     * @param xPix     the xPredicted monster position in speed (speed =number of frame)
     * @param yPix     the yPredicted monster position in speed (speed =number of frame)
     * @param root     the root
     * @param speed    the speed
     */
    public Shield(int towerKey, int xPix, int yPix, Pane root, int speed) {
            super(towerKey,xPix,yPix,speed);
            this.setName("Bouclier");
            this.setSpeed(speed);
            this.setSource("assets/attacks/shield.png");
    }
}
