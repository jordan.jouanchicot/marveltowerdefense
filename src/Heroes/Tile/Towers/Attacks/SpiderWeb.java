package Heroes.Tile.Towers.Attacks;

import javafx.scene.layout.Pane;

/**
 * The type Spider web (extends Attack)
 * @author Nicolas Mah-Chak
 */
public class SpiderWeb  extends Attack{

    /**
     * Instantiates a new Spider web.
     *
     * @param towerKey the tower key
     * @param xPix     the xPredicted monster position in speed (speed =number of frame)
     * @param yPix     the yPredicted monster position in speed (speed =number of frame)
     * @param root     the root
     * @param speed    the speed
     */
    public SpiderWeb(int towerKey, int xPix, int yPix, Pane root, int speed) {
            super(towerKey,xPix,yPix,root,speed);
            this.setName("SpiderWeb");
            this.setSpeed(speed);
            this.setSource("assets/attacks/spiderWeb.png");
    }

}
