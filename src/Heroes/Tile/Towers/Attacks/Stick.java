package Heroes.Tile.Towers.Attacks;

import javafx.scene.layout.Pane;

/**
 * The type Stick (extends Attack)
 * @author Nicolas Mah-Chak
 */
public class Stick  extends Attack {
    /**
     * Instantiates a new Stick.
     *
     * @param towerKey the tower key
     * @param xPix     the xPredicted monster position in speed (speed =number of frame)
     * @param yPix     the yPredicted monster position in speed (speed =number of frame)
     * @param root     the root
     * @param speed    the speed
     */
    public Stick(int towerKey, int xPix, int yPix, Pane root, int speed) {
        super(towerKey,xPix,yPix,root,speed);
        this.setName("Stick");
        this.setSpeed(speed);
        this.setSource("assets/attacks/stick.png");
    }

}
