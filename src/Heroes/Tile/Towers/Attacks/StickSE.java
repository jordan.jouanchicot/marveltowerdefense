package Heroes.Tile.Towers.Attacks;

import javafx.scene.layout.Pane;

/**
 * The type StickSE (extends Attack)
 * @author Vincent Urruty
 */
public class StickSE extends Attack {
    /**
     * Instantiates a new Stick special effect.
     *
     * @param towerKey the tower key
     * @param xPix     the xPredicted monster position in speed (speed =number of frame)
     * @param yPix     the yPredicted monster position in speed (speed =number of frame)
     * @param root     the root
     * @param speed    the speed
     */
    public StickSE(int towerKey, int xPix, int yPix, Pane root, int speed) {
        super(towerKey,xPix,yPix,root,speed);
        this.setName("StickSE");
        this.setSpeed(speed);
        this.setSource("assets/attacks/stickSE.png");
    }

}
