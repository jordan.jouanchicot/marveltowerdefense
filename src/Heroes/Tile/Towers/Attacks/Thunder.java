package Heroes.Tile.Towers.Attacks;

import javafx.scene.layout.Pane;

/**
 * The type Thunder (extends Attack)
 * @author Nicolas Mah-Chak
 */
public class Thunder extends Attack {
    /**
     * Instantiates a new Thunder.
     *
     * @param xPix     the xPredicted monster position in speed (speed =number of frame)
     * @param yPix     the yPredicted monster position in speed (speed =number of frame)
     * @param root           the root
     * @param speed          the speed
     * @param numberOfFrames the number of frames
     */
    public Thunder(int xPix, int yPix, Pane root, int speed, int numberOfFrames) {
        super(xPix, yPix, root, speed, numberOfFrames);
        this.setName("Thunder");
        this.setSpeed(speed);
        this.setSource("assets/attacks/thunder.png");
    }
}
