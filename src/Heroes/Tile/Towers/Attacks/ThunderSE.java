package Heroes.Tile.Towers.Attacks;

import javafx.scene.layout.Pane;

/**
 * The type ThunderSE (extends Attack)
 * @author Vincent Urruty
 */
public class ThunderSE extends Attack {
    /**
     * Instantiates a new Thunder Special Effect.
     *
     * @param xPix     the xPredicted monster position in speed (speed =number of frame)
     * @param yPix     the yPredicted monster position in speed (speed =number of frame)
     * @param root           the root
     * @param speed          the speed
     * @param numberOfFrames the number of frames
     */
    public ThunderSE(int xPix, int yPix, Pane root, int speed, int numberOfFrames) {
        super(xPix, yPix, root, speed, numberOfFrames);
        this.setName("ThunderSE");
        this.setSpeed(speed);
        this.setSource("assets/attacks/thunderSE.png");
    }
}
