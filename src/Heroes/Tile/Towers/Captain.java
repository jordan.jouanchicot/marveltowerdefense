package Heroes.Tile.Towers;

import Heroes.Monster.MonsterKinds;
import Heroes.Music.Music;
import Heroes.Tile.Towers.Attacks.Attack;
import Heroes.Tile.Towers.Attacks.Shield;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import Heroes.Game;
import Heroes.Monster.Monster;
import Heroes.Tile.Tile;
import Heroes.Tile.TileKinds;
import Heroes.Tile.TowerInfos;
import Heroes.Tile.TowerStates;

import java.util.ArrayList;

/**
 * The type Captain (extends Tower)
 * @author Jordan Rey-Jouanchicot
 */
public class Captain extends Tower {

    /**
     * Instantiates a new Captain.
     *
     * @param key the key
     */
    public Captain(int key) {
        // get infos from tower infos and from tower improvement and instatiate super class
        super(key% (Game.getWidth() / Tile.getTileSize()),
                (key-(key% (Game.getWidth() / Tile.getTileSize())))/(Game.getWidth() / Tile.getTileSize()),
                TileKinds.CAPTAIN,
                TowerStates.LEVEL1,
                Game.getUser().getTowersImprovements().towerImprovements(TowerInfos.CAPTAIN).getAttack()+TowerInfos.CAPTAIN.getAttack(),
                Game.getUser().getTowersImprovements().towerImprovements(TowerInfos.CAPTAIN).getAttackRate()+TowerInfos.CAPTAIN.getAttackRate(),
                Game.getUser().getTowersImprovements().towerImprovements(TowerInfos.CAPTAIN).getAttackRange()+TowerInfos.CAPTAIN.getAttackRange(),
                TowerInfos.CAPTAIN.getName(),
                TowerInfos.CAPTAIN.getPrice(),
                TowerInfos.CAPTAIN.getPrice()/2);
    }

    /**
     * increase the tower level if possible
     * @param root the pane
     * @return the tower with updated stats
     */
    public Tower incLevel(Pane root){
        // Update the tower stats
        this.setState(this.getState().inc());
        this.setAttack((int) (getAttack()+500));
        this.setAttackRange((getAttackRange()+Tile.getTileSize()));
        Game.getMap().render(getKey(), root);
        this.setSellPrice(this.getBuyPrice()*this.getState().getTowerState()/2);
        return this;
    }

    public void render(Pane root){
        // Render tower depending of it's level
        if (getState()==TowerStates.LEVEL1){
            Game.getMap().render(getKey(),root,TowerInfos.CAPTAIN.getAssetPath());
        }
        else if (getState()==TowerStates.LEVEL2){
            Game.getMap().render(getKey(),root,"/assets/towers/ToCapt2D.png");
        }
        else{
            Game.getMap().render(getKey(),root,"/assets/towers/ToCapt3D.png");
        }
    }


    public void attack(Monster monster, Pane root) {

        int monsterx = monster.getxList();
        int monstery= monster.getyList();

        double distance = Math.sqrt(Math.pow((getPosX() -monsterx),2) + Math.pow((getPosY() -monstery),2));
        // if distance tower monster < range et monster not death
        if (!monster.getDeath() && distance <=this.getAttackRange()) {
            // set monster attacking at this loop
            this.setAttacking(true);
            // update life of monster and set it to death if it's the case
            monster.setLifePoints(monster.getLifePoints() - this.getAttack());
            if(monster.getLifePoints() <=0) monster.setDeath(true);
            int positionx;
            int positiony;
            // make attack animation (move to monster position in 3 frame (if more than 3 poisition in Lists), in 3 frame)
            if (monster.getListX().size() >3){
                positionx = monster.getListX().get(3);
                positiony = monster.getListY().get(3);
            }
            else{
                positionx = monster.getListX().get(monster.getListX().size()-1);
                positiony = monster.getListY().get(monster.getListX().size()-1);
            }
            Attack attack = new Shield(getKey(),positionx,positiony,root,3);
            attack.render(root,attack.getSource());
            setAttackTower(attack);
            // add this to the attacklist in game (list of attack that handle in gameloop)
            Game.getAttackList().add(this.getAttackTower());
            // try special attack
            specialAttack(monster);
        }
    }

    /**
     * Special attack : go back at beginning
     * @param monster the monster attacked
     */
    public void specialAttack(Monster monster){
        // 1/6 and if not monster resistant to it, make special attack got at beginning (clear path, and recalculate path from beginning at his speed)
        if ((Math.random()*6)<1 && (monster.getMonsterKind() != MonsterKinds.TITAN)) {
            ArrayList<Integer> pathT = new ArrayList<>(Game.getMap().getPathList());
            monster.clearPath();
            monster.pathLinearize(pathT, 0,0, monster.getSpeed());
            monster.monsterMove(pathT);
        }
    };


}
