package Heroes.Tile.Towers;

import Heroes.Monster.Monster;
import Heroes.Monster.MonsterKinds;
import Heroes.Tile.Towers.Attacks.Attack;
import Heroes.Tile.Towers.Attacks.Thunder;
import Heroes.Tile.Towers.Attacks.ThunderSE;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import Heroes.Game;
import Heroes.Tile.Tile;
import Heroes.Tile.TileKinds;
import Heroes.Tile.TowerInfos;
import Heroes.Tile.TowerStates;


/**
 * The type God (extends Tower)
 *
 * @author Jordan Rey-Jouanchicot
 */
public class God extends Tower {
    private boolean isSpecialAttacking = false;
    /**
     * Instantiates a new God.
     *
     * @param key the key
     */
    public God(int key) {
        super(key% (Game.getWidth() / Tile.getTileSize()),
                (key-(key% (Game.getWidth() / Tile.getTileSize())))/(Game.getWidth() / Tile.getTileSize()),
                TileKinds.GOD,
                TowerStates.LEVEL1,
                Game.getUser().getTowersImprovements().towerImprovements(TowerInfos.GOD).getAttack()+TowerInfos.GOD.getAttack(),
                Game.getUser().getTowersImprovements().towerImprovements(TowerInfos.GOD).getAttackRate()+TowerInfos.GOD.getAttackRate(),
                Game.getUser().getTowersImprovements().towerImprovements(TowerInfos.GOD).getAttackRange()+TowerInfos.GOD.getAttackRange(),
                TowerInfos.GOD.getName(),
                TowerInfos.GOD.getPrice(),
                TowerInfos.GOD.getPrice()/2);
    }

    public Tower incLevel(Pane root){
        // Update the tower stats
        this.setState(this.getState().inc());
        this.setAttack((int) (getAttack()+1000));
        this.setAttackRate((getAttackRate()+3));
        this.setAttackRange((getAttackRange()+Tile.getTileSize()));
        Game.getMap().render(getKey(), root);
        this.setSellPrice(this.getBuyPrice()*this.getState().getTowerState()/2);
        return this;
    }

    public void render(Pane root){
        // Render tower depending of it's level
        if (getState()==TowerStates.LEVEL1){
            Game.getMap().render(getKey(),root,TowerInfos.GOD.getAssetPath());
        }
        else if (getState()==TowerStates.LEVEL2){
            Game.getMap().render(getKey(),root,"/assets/towers/ToThor2D.png");
        }
        else{
            Game.getMap().render(getKey(),root,"/assets/towers/ToThor3D.png");
        }
    }


    public void attack(Monster monster, Pane root) {
        // If tower is not super attacking try to make classic attack
        if (!isSpecialAttacking) {
            int monsterx = monster.getxList();
            int monstery = monster.getyList();

            // if monster not death and distance < range, set attacking to true, update monster lifepoints and deathstate and make attack animation
            if (!monster.getDeath() && (Math.sqrt(Math.pow((getPosX() - monsterx), 2) + Math.pow((getPosY() - monstery), 2))) <= this.getAttackRange()) {
                this.setAttacking(true);
                monster.setLifePoints(monster.getLifePoints() - this.getAttack());
                if (monster.getLifePoints() <= 0) monster.setDeath(true);
                int numberOfFrames = 5;
                int positionx = monster.getListX().get(numberOfFrames);
                int positiony = monster.getListY().get(numberOfFrames);

                Attack attack = new Thunder(positionx, positiony, root, 5, numberOfFrames);
                attack.render(root, attack.getSource());
                setAttackTower(attack);
                Game.getAttackList().add(this.getAttackTower());
            }
            // Try to make attack special (1/3 expected value)
            if ((Math.random() * 3) < 1) {
                specialAttack(monster, root);
                this.setAttacking(false);
                isSpecialAttacking = true;
            }
        }
        else {
            // if isSpecial attacking try to make attack special (1/3 expected value) (ie : attack all the monsters not treated yet at the main loop, that are in the range)
            if ((Math.random() * 3) < 1) {
                specialAttack(monster, root);
            }
        }
    }


    /**
     * Special attack.
     *
     * @param monster the monster attacked
     * @param root    the root
     */
    public void specialAttack(Monster monster,Pane root) {
        int monsterx = monster.getxList();
        int monstery = monster.getyList();

        // if monster not death and distance < range*3, set attacking to true, update monster lifepoints and deathstate and make special attack animation
        if (!monster.getDeath() && ((Math.sqrt(Math.pow((getPosX() - monsterx), 2) + Math.pow((getPosY() - monstery), 2))) <= this.getAttackRange()*3) && ((monster.getMonsterKind() != MonsterKinds.MAL))) {

            monster.setLifePoints(monster.getLifePoints() - this.getAttack());
            if (monster.getLifePoints() <= 0) monster.setDeath(true);
            int numberOfFrames = 5;
            int positionx = monster.getListX().get(numberOfFrames);
            int positiony = monster.getListY().get(numberOfFrames);

            Attack attack = new ThunderSE(positionx, positiony, root, 5, numberOfFrames);
            attack.render(root, attack.getSource());
            setAttackTower(attack);
            // add this to the attacklist in game (list of attack that have to be handle in gameloop)
            Game.getAttackList().add(this.getAttackTower());
        }
    }

    /**
     * get isSpecialAttacking
     * @return if monster is special attacking or not
     */
    public boolean isSpecialAttacking() {
        return isSpecialAttacking;
    }

    /**set special attacking boolean
     */
    public void setSpecialAttacking(boolean specialAttacking) {
        isSpecialAttacking = specialAttacking;
    }
}
