package Heroes.Tile.Towers;

import Heroes.Monster.MonsterKinds;
import Heroes.Tile.Towers.Attacks.Attack;
import Heroes.Tile.Towers.Attacks.Laser;
import Heroes.Tile.Towers.Attacks.Shield;
import Heroes.Tile.Towers.Attacks.SpiderWeb;
import javafx.application.Platform;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import Heroes.Game;
import Heroes.Monster.Monster;
import Heroes.Tile.*;

import java.util.ArrayList;

import static java.lang.Integer.max;

/**
 * The type Green lizard (extends Tower)
 * @author Jordan Rey-Jouanchicot
 */
public class GreenLizard extends Tower {

    /**
     * Instantiates a new Green lizard.
     *
     * @param key the key
     */
    public GreenLizard(int key) {
        super(key % (Game.getWidth() / Tile.getTileSize()),
                (key - (key % (Game.getWidth() / Tile.getTileSize()))) / (Game.getWidth() / Tile.getTileSize()),
                TileKinds.GREENLIZARD,
                TowerStates.LEVEL1,
                Game.getUser().getTowersImprovements().towerImprovements(TowerInfos.GREENLIZARD).getAttack()+TowerInfos.GREENLIZARD.getAttack(),
                Game.getUser().getTowersImprovements().towerImprovements(TowerInfos.GREENLIZARD).getAttackRate()+TowerInfos.GREENLIZARD.getAttackRate(),
                Game.getUser().getTowersImprovements().towerImprovements(TowerInfos.GREENLIZARD).getAttackRange()+TowerInfos.GREENLIZARD.getAttackRange(),
                TowerInfos.GREENLIZARD.getName(),
                TowerInfos.GREENLIZARD.getPrice(),
                TowerInfos.GREENLIZARD.getPrice() / 2);
    }


    public Tower incLevel(Pane root) {
        // Update the tower stats
        this.setState(this.getState().inc());
        this.setAttack((int) (getAttack() + 300));
        this.setAttackRate((getAttackRate() + 5));
        Game.getMap().render(getKey(), root);
        this.setSellPrice(this.getBuyPrice()*this.getState().getTowerState()/2);
        return this;
    }

    public void render(Pane root) {
        // Render tower depending of it's level
        if (getState() == TowerStates.LEVEL1) {
            Game.getMap().render(getKey(),root, TowerInfos.GREENLIZARD.getAssetPath());
        }
        else if (getState() == TowerStates.LEVEL2) {
            Game.getMap().render(getKey(),root, "/assets/towers/ToSpi2D.png");
        } else {
            Game.getMap().render(getKey(),root, "/assets/towers/ToSpi3D.png");
        }
    }

    public void attack(Monster monster, Pane root) {

        int monsterx = monster.getxList();
        int monstery= monster.getyList();

        double distance = Math.sqrt(Math.pow((getPosX() -monsterx),2) + Math.pow((getPosY() -monstery),2));

        // if distance tower monster < range et monster not death
        // set attacking to true, update monster lifepoints and deathstate and make attack animation
        if (!monster.getDeath() && distance <=this.getAttackRange()) {
            this.setAttacking(true);
            monster.setLifePoints(monster.getLifePoints() - this.getAttack());
            if(monster.getLifePoints() <=0) monster.setDeath(true);
            int positionx;
            int positiony;
            if (monster.getListX().size() >3){
                 positionx = monster.getListX().get(3);
                 positiony = monster.getListY().get(3);
            }
            else{
                positionx = monster.getListX().get(monster.getListX().size()-1);
                positiony = monster.getListY().get(monster.getListX().size()-1);
            }
            Attack attack = new SpiderWeb(getKey(),positionx,positiony,root,3);
            attack.render(root,attack.getSource());
            setAttackTower(attack);
            Game.getAttackList().add(this.getAttackTower());
            // try special attack
            specialAttack(monster);
        }
    }

    /**
     * Special attack.
     *
     * @param monster the monster
     */
    public void specialAttack(Monster monster){
        // if possible and (1/2) time, make attack special, recalculate path of enemy but with speed-1, to make it move slower
        if (((Math.random()*2)<1)&&(monster.getSpeed()>2) && (!monster.getDeath())&& (monster.getMonsterKind() != MonsterKinds.DROID)) {
            ArrayList<Integer> xListA = new ArrayList<>(monster.getListX());
            ArrayList<Integer> yListA = new ArrayList<>(monster.getListY());
            monster.getListY().clear();
            monster.getListX().clear();
            monster.pathLinearizeXY(xListA,yListA, 0,0, max(1,(monster.getSpeed()-1)));
            monster.setSpeed(monster.getSpeed()/2);
        }
    }
}
