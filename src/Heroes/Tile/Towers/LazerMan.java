package Heroes.Tile.Towers;

import Heroes.Monster.Monster;
import Heroes.Monster.MonsterKinds;
import Heroes.Music.Music;
import Heroes.Tile.Towers.Attacks.Attack;
import Heroes.Tile.Towers.Attacks.Laser;
import Heroes.Tile.Towers.Attacks.LaserSE;
import Heroes.Tile.Towers.Attacks.SpiderWeb;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import Heroes.Game;
import Heroes.Tile.Tile;
import Heroes.Tile.TileKinds;
import Heroes.Tile.TowerInfos;
import Heroes.Tile.TowerStates;

/**
 * The type Lazer man (extends Tower)
 * @author Jordan Rey-Jouanchicot
 */
public class LazerMan extends Tower {

    /**
     * Instantiates a new Lazer man.
     *
     * @param key the key
     */
    public LazerMan(int key) {
        super(key % (Game.getWidth() / Tile.getTileSize()),
                (key - (key % (Game.getWidth() / Tile.getTileSize()))) / (Game.getWidth() / Tile.getTileSize()),
                TileKinds.LAZERMAN,
                TowerStates.LEVEL1,
                Game.getUser().getTowersImprovements().towerImprovements(TowerInfos.LAZERMAN).getAttack() + TowerInfos.LAZERMAN.getAttack(),
                Game.getUser().getTowersImprovements().towerImprovements(TowerInfos.LAZERMAN).getAttackRate() + TowerInfos.LAZERMAN.getAttackRate(),
                Game.getUser().getTowersImprovements().towerImprovements(TowerInfos.LAZERMAN).getAttackRange() + TowerInfos.LAZERMAN.getAttackRange(),
                TowerInfos.LAZERMAN.getName(),
                TowerInfos.LAZERMAN.getPrice(),
                TowerInfos.LAZERMAN.getPrice() / 2);
    }

    public Tower incLevel(Pane root) {
        // Update the tower stats
        this.setState(this.getState().inc());
        this.setAttack((int) (getAttack() + 400));
        this.setAttackRate((getAttackRate() + 10));
        Game.getMap().render(getKey(), root);
        this.setSellPrice(this.getBuyPrice()*this.getState().getTowerState()/2);
        return this;
    }


    public void render(Pane root){
        // Render tower depending of it's level
        if (getState()==TowerStates.LEVEL1){
            Game.getMap().render(getKey(),root, TowerInfos.LAZERMAN.getAssetPath());
        }
        else if (getState() == TowerStates.LEVEL2) {
            Game.getMap().render(getKey(),root, "/assets/towers/ToIron2D.png");
        }
        else {
            Game.getMap().render(getKey(),root, "/assets/towers/ToIron3D.png");
        }
    }
    public void attack(Monster monster, Pane root) {

        int monsterx = monster.getxList();
        int monstery = monster.getyList();

        double distance = Math.sqrt(Math.pow((getPosX() - monsterx), 2) + Math.pow((getPosY() - monstery), 2));

        // if distance tower monster < range et monster not death
        // set attacking to true, update monster lifepoints and deathstate and make attack animation (special attack)
        if (!monster.getDeath() && distance <= this.getAttackRange()) {
            this.setAttacking(true);
            monster.setLifePoints(monster.getLifePoints() - this.getAttack());
            if (monster.getLifePoints() <= 0) monster.setDeath(true);
            int positionx;
            int positiony;
            if (monster.getListX().size() >3){
                positionx = monster.getListX().get(3);
                positiony = monster.getListY().get(3);
            }
            else{
                positionx = monster.getListX().get(monster.getListX().size()-1);
                positiony = monster.getListY().get(monster.getListX().size()-1);
            }
            Attack attack = new LaserSE(getKey(),positionx,positiony,root,3);
            attack.render(root, attack.getSource());
            setAttackTower(attack);
            Game.getAttackList().add(this.getAttackTower());
        }
        // try special attack
        if (!this.isAttacking()) specialAttack(monster, root);
    }

    /**
     * Special attack.
     *
     * @param monster the monster attacked
     * @param root    the root
     */
    public void specialAttack(Monster monster,Pane root) {

        int monsterx = monster.getxList();
        int monstery = monster.getyList();
        double distance = Math.sqrt(Math.pow((getPosX() - monsterx), 2) + Math.pow((getPosY() - monstery), 2));

        // if monster not death and distance < range*3, and monster not resitant, set attacking to true, update monster lifepoints and deathstate and make attack animation
        if (((Math.random() * 8) < 1)&&(!monster.getDeath()) && (distance <= this.getAttackRange()*3) && (monster.getMonsterKind() != MonsterKinds.DEVIL)) {
            this.setAttacking(true);
            monster.setLifePoints(monster.getLifePoints() - this.getAttack() * 2);
            if (monster.getLifePoints() <= 0) monster.setDeath(true);
            int place = (int) distance / (monster.getSpeed() * this.getAttackSpeed() * 20);
            int positionx = monster.getListX().get(place);
            int positiony = monster.getListY().get(place);
            Attack attack = new Laser(getKey(), positionx, positiony, root, 10);
            attack.render(root, attack.getSource());
            setAttackTower(attack);
            Game.getAttackList().add(this.getAttackTower());
        }
    }

}
