package Heroes.Tile.Towers;

import Heroes.Tile.Towers.Attacks.Attack;
import javafx.scene.layout.Pane;
import Heroes.Game;
import Heroes.Map.MapOfTiles;
import Heroes.Monster.Monster;
import Heroes.Tile.Constructible;
import Heroes.Tile.Tile;
import Heroes.Tile.TileKinds;
import Heroes.Tile.TowerStates;

import java.util.ArrayList;

/**
 * The type Tower (extends Constructible)
 * @author Jordan Rey-Jouanchicot
 */
public abstract class Tower extends Constructible {
    // Attributes
    private String towerName;
    private int buyPrice;
    private int sellPrice;
    private int posX,posY;
    private int key;
    private boolean attacking = false;
    private Attack attackTower;

    /**
     * Instantiates a new Tower.
     *
     * @param x           the x
     * @param y           the y
     * @param tileKind    the tile kind
     * @param state       the state
     * @param attack      the attack
     * @param attackRate  the attack rate
     * @param attackRange the attack range
     * @param towerName   the tower name
     * @param buyPrice    the buy price
     * @param sellPrice   the sell price
     */
    public Tower(int x, int y, TileKinds tileKind, TowerStates state, int attack, int attackRate, int attackRange, String towerName, int buyPrice, int sellPrice) {
        super(x, y, tileKind, state, attack, attackRate, attackRange);
        this.towerName = towerName;
        this.buyPrice = buyPrice;
        this.sellPrice = sellPrice;
    }

    /**
     * Get damage value
     *
     * @return the value(int)
     */
    public int getDamage(){return this.getAttack();}

    /**
     * Get attack value(int)
     *
     * @return the value(int)
     */
    public int getAttackSpeed(){ return this.getAttackRate();}

    /**
     * Get range value(int)
     *
     * @return the value(int)
     */
    public int getRange(){return  this.getAttackRange();}


    /**
     * Gets buy price.
     *
     * @return the buyPrice(int)
     */
    public int getBuyPrice() {
        return buyPrice;
    }

    /**
     * Get name string.
     *
     * @return the name
     */
    public String getName(){return towerName;}

    /**
     * Gets sell price.
     *
     * @return the sell price
     */
    public int getSellPrice() {
        return this.sellPrice;
    }

    /**
     * Increase level of tower.
     *
     * @param root the root
     * @return the tower
     */
    public abstract Tower incLevel(Pane root);

    /**
     * Setp position from key
     *
     * @param keyPrev the key prev
     */
    public void setpos(int keyPrev){
        // set position of a tower from the key translate key value to x,y pixels coordinates
        this.key = keyPrev;
        int xPrev = keyPrev%(Game.getWidth() /Tile.getTileSize());
        int  yPrev = (keyPrev - xPrev) / (Game.getWidth() / Tile.getTileSize());

        int yPrevPix = -Game.getWidth() / 2 + (yPrev) * Tile.getTileSize() + MapOfTiles.getHeightShift();
        int xPrevPix = -Game.getHeight() / 2 + (xPrev) * Tile.getTileSize()+ MapOfTiles.getWidthShift();
        // store in this x and y position from calculates one
        posX = xPrevPix;
        posY = yPrevPix;

    }


    /**
     * Attack of a tower
     *
     * @param monster the monster attacked
     * @param root    the root
     */
    public abstract void attack(Monster monster, Pane root);


    /**
     * Is attacking boolean.
     *
     * @return the boolean
     */
    public boolean isAttacking() {
        return attacking;
    }

    /**
     * Sets attacking.
     *
     * @param attacking the attacking
     */
    public void setAttacking(boolean attacking) {
        this.attacking = attacking;
    }

    /**
     * Gets PositionX
     *
     * @return the PositionX
     */
    public int getPosX() {
        return posX;
    }

    /**
     * Gets PositionY
     *
     * @return the PositionY
     */
    public int getPosY() {
        return posY;
    }

    /**
     * Gets key.
     *
     * @return the key
     */
    public int getKey() {
        return key;
    }

    /**
     * Sets key.
     *
     * @param key the key
     */
    public void setKey(int key) {
        this.key = key;
    }

    /**
     * Gets attack tower.
     *
     * @return the attackTower
     */
    public Attack getAttackTower() {
        return attackTower;
    }

    /**
     * Sets attack tower.
     *
     * @param attackTower the attackTower
     */
    public void setAttackTower(Attack attackTower) {
        this.attackTower = attackTower;
    }

    /**
     * get isSpecialAttacking
     * @return always false if not override
     */
    public boolean isSpecialAttacking() {
        return false;
    }

    /**
     * set special attacking boolean (used to allow this to god)
     * if not override does nothing
     * Does nothing, only use it for override version
     * @param  specialAttacking don't matter in not override function
     */
    public void setSpecialAttacking(boolean specialAttacking) {}

    /***
     * set sellPrice
     * @param sellPrice the sellprice value to set
     */
    public void setSellPrice(int sellPrice) {
        this.sellPrice = sellPrice;
    }
}
