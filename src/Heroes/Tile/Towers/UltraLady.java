package Heroes.Tile.Towers;

import Heroes.Monster.Monster;
import Heroes.Music.Music;
import Heroes.Tile.Towers.Attacks.Attack;
import Heroes.Tile.Towers.Attacks.Laser;
import Heroes.Tile.Towers.Attacks.Stick;
import Heroes.Tile.Towers.Attacks.StickSE;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import Heroes.Game;
import Heroes.Tile.Tile;
import Heroes.Tile.TileKinds;
import Heroes.Tile.TowerInfos;
import Heroes.Tile.TowerStates;

/**
 * The type Ultra lady (extends Tower)
 * @author Jordan Rey-Jouanchicot
 */
public class UltraLady extends Tower {

    /**
     * Instantiates a new Ultra lady.
     *
     * @param key the key
     */
    public UltraLady(int key) {
        super(key% (Game.getWidth() / Tile.getTileSize()),
                //Game.
                (key-(key% (Game.getWidth() / Tile.getTileSize())))/(Game.getWidth() / Tile.getTileSize()),
                TileKinds.ULTRALADY,
                TowerStates.LEVEL1,
                Game.getUser().getTowersImprovements().towerImprovements(TowerInfos.ULTRALADY).getAttack()+TowerInfos.ULTRALADY.getAttack(),
                Game.getUser().getTowersImprovements().towerImprovements(TowerInfos.ULTRALADY).getAttackRate()+TowerInfos.ULTRALADY.getAttackRate(),
                Game.getUser().getTowersImprovements().towerImprovements(TowerInfos.ULTRALADY).getAttackRange()+TowerInfos.ULTRALADY.getAttackRange(),
                TowerInfos.ULTRALADY.getName(),
                TowerInfos.ULTRALADY.getPrice(),
                TowerInfos.ULTRALADY.getPrice()/2);
    }

    public Tower incLevel(Pane root){
        // Update the tower stats
        this.setState(this.getState().inc());
        this.setAttack((int) (getAttack()+200));
        this.setAttackRate((getAttackRate()+10));
        Game.getMap().render(getKey(), root);
        this.setSellPrice(this.getBuyPrice()*this.getState().getTowerState()/2);
        return this;
    }

    public void render(Pane root){
        // Render tower depending of it's level
        if (getState()==TowerStates.LEVEL1){
            Game.getMap().render(getKey(),root, TowerInfos.ULTRALADY.getAssetPath());
        }
        else if (getState()==TowerStates.LEVEL2){
            Game.getMap().render(getKey(),root,"/assets/towers/ToBW2D.png");
        }
        else{
            Game.getMap().render(getKey(),root,"/assets/towers/ToBW3D.png");
        }
    }

    public void attack(Monster monster, Pane root) {

        int monsterx = monster.getxList();
        int monstery= monster.getyList();

        double distance = Math.sqrt(Math.pow((getPosX() -monsterx),2) + Math.pow((getPosY() -monstery),2));

        // if distance tower monster < range et monster not death
        // set attacking to true, update monster lifepoints and deathstate and make attack animation
        if (!monster.getDeath() && distance <=this.getAttackRange()) {
            this.setAttacking(true);
            monster.setLifePoints(monster.getLifePoints() - this.getAttack());
            if(monster.getLifePoints() <=0) monster.setDeath(true);
            int positionx;
            int positiony;
            if (monster.getListX().size() >3){
                positionx = monster.getListX().get(3);
                positiony = monster.getListY().get(3);
            }
            else{
                positionx = monster.getListX().get(monster.getListX().size()-1);
                positiony = monster.getListY().get(monster.getListX().size()-1);
            }
            Attack attack = new Stick(getKey(),positionx,positiony,root,3);
            attack.render(root,attack.getSource());
            setAttackTower(attack);
            Game.getAttackList().add(this.getAttackTower());
         }
        // try to make special attack (1/8) and not attacking at this loop
        if ((!this.isAttacking())&&((Math.random() * 8) < 1)) specialAttack(monster, root);
    }

    /**
     * Special attack.
     *
     * @param monster the monster
     * @param root    the root
     */
    public void specialAttack(Monster monster,Pane root) {

        int monsterx = monster.getxList();
        int monstery = monster.getyList();
        double distance = Math.sqrt(Math.pow((getPosX() - monsterx), 2) + Math.pow((getPosY() - monstery), 2));
        // if not death and range is okay make special attack (same code than attack but with bigger range and bigger damage) and not the same image
        if ((!monster.getDeath())&&(distance <=this.getAttackRange()*1.5)) {
            this.setAttacking(true);

            monster.setLifePoints(monster.getLifePoints() - this.getAttack() * 2);
            if (monster.getLifePoints() <= 0) monster.setDeath(true);
            int positionx;
            int positiony;
            if (monster.getListX().size() >3){
                positionx = monster.getListX().get(3);
                positiony = monster.getListY().get(3);
            }
            else{
                positionx = monster.getListX().get(monster.getListX().size()-1);
                positiony = monster.getListY().get(monster.getListX().size()-1);
            }
            Attack attack = new StickSE(getKey(),positionx,positiony,root,3);
            attack.render(root, attack.getSource());
            setAttackTower(attack);
            Game.getAttackList().add(this.getAttackTower());
        }
    }

}
