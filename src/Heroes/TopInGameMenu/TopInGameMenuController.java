package Heroes.TopInGameMenu;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import Heroes.Game;
import Heroes.Music.Music;

import java.net.URL;
import java.util.ResourceBundle;


/**
 * The type Top in game menu controller
 * @author Jordan Rey-Jouanchicot
 */
public class TopInGameMenuController implements Initializable {

    /**
     * The button to handle music
     */
    @FXML Button bt2;
    /**
     * The button to play and pause game
     */
    @FXML Button bt3;
    /**
     * The H box.
     */
    @FXML HBox hBox;
    /**
     * The label binded with money variable of User
     */
    @FXML Label lb1;
    /**
     * The label binded with money variable of lifes
     */
    @FXML Label lb2;
    /**
     * The label binded with money variable of wave
     */
    @FXML Label lb3;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        bt3.setText("Démarrer partie");
        bt2.setText("Stop musique");
        lb1.setText("Argent = " );
        lb2.setText("Score = ");
        lb1.textProperty().bind((Game.getInGameUser().getMoneyP()).asString("Argent = %d"));
        lb2.textProperty().bind((Game.getInGameUser().getLifesP()).asString("Vies = %d"));
        lb3.textProperty().bind((Game.getInGameUser().getWaveP()).asString("Vague  = %d"));
        Game.getMusic().ambiance();
    }


    /**
     * On mouse clicked music.
     */
    public void onMouseClickedMusic(){
        if ( Game.getMusic().isPlaying()){
            // Si musique joue pause musiquee
            bt2.setText("Jouer musique");
            Game.getMusic().stop();
        }
        else{
            // sinon play musique ambiance
            Game.setMusic(new Music("ambiance.mp3"));
            Game.getMusic().ambiance();
            bt2.setText("Stop musique");
        }
    }

    /**
     * On mouse clicked play pause.
     */
    public void onMouseClickedPlayPause(){
        // si jeu en pause, mettre play sinon pause
        if (Game.isPaused()){
            Game.setPaused(false);
            bt3.setText("Pause");
        }
        else{
            Game.setPaused(true);
            if (Game.getMusic().isPlaying()){
                bt2.setText("Jouer musique");
                Game.getMusic().stop();
            }
            bt3.setText("Jouer");
        }
    }


}
