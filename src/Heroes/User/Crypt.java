package Heroes.User;


/**
 * The type Crypt, based on decrypt but making use more easy
 * @author Jordan Rey-Jouanchicot
 */
public class Crypt {
    private String hashed;

    /**
     * Instantiates a new Crypt.
     *
     * @param s the s
     */
    public Crypt(String s){
        this.hashed = s;
    }

    /**
     * Encrypt.
     */
    public void encrypt() {
        this.hashed = BCrypt.hashpw(this.hashed, BCrypt.gensalt(12));
    }

    /**
     * Equals boolean.
     *
     * @param notEncrypted the not encrypted
     * @return the boolean
     */
    public boolean equals(Crypt notEncrypted){
        return (BCrypt.checkpw(hashed,notEncrypted.hashed));
    }

    @Override
    public String toString() {
        return "Crypt{" +
                "hashed='" + hashed + '\'' +
                '}';
    }
}
