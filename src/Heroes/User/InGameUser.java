package Heroes.User;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

/**
 * The type InGameUser.
 * @author Jordan Rey-Jouanchicot
 */
public class InGameUser {
    // All attribute of a user, they are binded to interface
    private IntegerProperty wave = new SimpleIntegerProperty(1);
    private IntegerProperty  lifes =  new SimpleIntegerProperty(25);
    private IntegerProperty money = new SimpleIntegerProperty(2000);

    /**
     * Instantiates a new In game user.
     */
    public InGameUser() {}

    /**
     * Gets wave value property.
     *
     * @return the wave value Integerproperty
     */
    public IntegerProperty getWaveP() {
        return wave;
    }

    /**
     * Gets wave value.
     *
     * @return the wave value
     */
    public int getWave() {
        return wave.getValue();
    }

    /**
     * Sets wave value
     *
     * @param wave the wave value
     */
    public void setWaveNumber(int wave) {
        this.wave.setValue(wave);
    }


    /**
     * Gets lifes value
     *
     * @return the lifes value
     */
    public int getLifes() {
        return lifes.getValue();
    }

    /**
     * Take lifes.
     *
     * @param lifes the lifes which will be taken
     */
    public void takeLifes(int lifes) {
        this.lifes.setValue( this.lifes.getValue()-lifes);
    }

    /**
     * Gets lifes property
     *
     * @return the lifes property
     */
    public IntegerProperty getLifesP() {
        return this.lifes;
    }

    /**
     * Gets money value
     *
     * @return the money value
     */
    public int getMoney() {
        return money.getValue();
    }

    /**
     * Gets money property
     *
     * @return the money property
     */
    public IntegerProperty getMoneyP() {
        return money;
    }

    /**
     * Add money to the actual value of money
     *
     * @param money the money to add
     */
    public void addMoney(int money) {
        this.money.setValue( this.money.getValue()+money);
    }

    /**
     * Take money from the actual money
     *
     * @param money the money to take
     */
    public void takeMoney(int money) {
        this.money.setValue(this.money.getValue()- money);
    }
}
