package Heroes.User;

import Heroes.Tile.Tile;
import Heroes.Tile.TowerInfos;

/**
 * The type Tower improvement.
 * @author Jordan Rey-Jouanchicot
 */
public class TowerImprovement {
    private int attack;
    private int attackRate;
    private int attackRange;
    private TowerInfos towerInfos;

    // All static infos
    private static final int valAttack = 10;
    private static final int valAttackRange = Tile.getTileSize()/15;
    private static final int valAttackRate = 1;
    private static final int costAttack = 300;
    private static final int costAttackRange = 1000;
    private static final int costAttackRate = 2000;
    private static final int maxNumberOfEach = 10;


    /**
     * Instantiates a new Tower improvement.
     *
     * @param tw the towerInfos
     */
    public TowerImprovement(TowerInfos tw){
        this.towerInfos = tw;
        this.attack = 0;
        this.attackRate =0;
        this.attackRange =0;
    }

    /**
     * Gets val attack.
     *
     * @return the val attack
     */
    public static int getValAttack() {
        return valAttack;
    }

    /**
     * Gets val attack range.
     *
     * @return the val attack range
     */
    public static int getValAttackRange() {
        return valAttackRange;
    }

    /**
     * Gets val attack rate.
     *
     * @return the val attack rate
     */
    public static int getValAttackRate() {
        return valAttackRate;
    }

    /**
     * Gets cost attack.
     *
     * @return the cost attack
     */
    public static int getCostAttack() {
        return costAttack;
    }

    /**
     * Gets cost attack range.
     *
     * @return the cost attack range
     */
    public static int getCostAttackRange() {
        return costAttackRange;
    }

    /**
     * Gets cost attack rate.
     *
     * @return the cost attack rate
     */
    public static int getCostAttackRate() {
        return costAttackRate;
    }

    /**
     * Gets max number of each.
     *
     * @return the max number of each
     */
    public static int getMaxNumberOfEach() {
        return maxNumberOfEach;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return towerInfos.getName();
    }

    /**
     * Gets tower infos.
     *
     * @return the tower infos
     */
    public TowerInfos getTowerInfos() {return towerInfos;}

    /**
     * Gets attack.
     *
     * @return the attack
     */
    public int getAttack() {
        return attack;
    }

    /**
     * Sets attack.
     *
     * @param attack the attack
     */
    public void setAttack(int attack) {
        this.attack = attack;
    }

    /**
     * Gets attack rate.
     *
     * @return the attack rate
     */
    public int getAttackRate() {
        return attackRate;
    }

    /**
     * Sets attack rate.
     *
     * @param attackRate the attack rate
     */
    public void setAttackRate(int attackRate) {
        this.attackRate = attackRate;
    }

    /**
     * Gets attack range.
     *
     * @return the attack range
     */
    public int getAttackRange() {
        return attackRange;
    }

    /**
     * Sets attack range.
     *
     * @param attackRange the attack range
     */
    public void setAttackRange(int attackRange) {
        this.attackRange = attackRange;
    }
}
