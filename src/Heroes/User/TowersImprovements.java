package Heroes.User;

import Heroes.Tile.TowerInfos;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * The type Towers improvements.
 * @author Jordan Rey-Jouanchicot
 */
public class TowersImprovements {

    private HashMap<TowerInfos, TowerImprovement> mapTkTowersImprovement = new HashMap<>();

    /**
     * Instantiates a new Towers improvements.
     */
    public TowersImprovements(){
        ArrayList<TowerInfos> towerlist = new ArrayList<>();
        towerlist.addAll(Stream.of(TowerInfos.values()).collect(Collectors.toList()));

        for (TowerInfos towInfo: towerlist) {
            mapTkTowersImprovement.put(towInfo,new TowerImprovement(towInfo));
        }
    }

    /**
     * Tower improvements tower improvement.
     *
     * @param tw the towerInfos
     * @return the tower improvement
     */
    public TowerImprovement towerImprovements(TowerInfos tw){
        return mapTkTowersImprovement.get(tw);
    }


    /**
     * Gets map tileInfos towers improvement.
     *
     * @return the map tileInfos towers improvement
     */
    public HashMap<TowerInfos, TowerImprovement> getMapTkTowersImprovement() {
        return mapTkTowersImprovement;
    }

}
