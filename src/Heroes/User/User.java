package Heroes.User;

import com.google.gson.Gson;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;

/**
 * The type User.
 */
public class User {

    private int levelMax;
    private int totalWavesNumber;
    private int numberGamesPlayed;

    private int level;
    private int exp;
    private int money;
    private String name;
    private TowersImprovements towersImprovements;

    private Crypt cryptedVal;


    /**
     * Instantiates a new User.
     */
    public User() {}

    /**
     * Instantiates a new User.
     *
     * @param levelMax          the level max
     * @param totalWavesNumber  the total waves number
     * @param numberGamesPlayed the number games played
     * @param level             the level
     * @param exp               the exp
     * @param money             the money
     * @param name              the name
     */
    public User(int levelMax, int totalWavesNumber, int numberGamesPlayed, int level, int exp, int money, String name) {
        this.levelMax = levelMax;
        this.totalWavesNumber = totalWavesNumber;
        this.numberGamesPlayed = numberGamesPlayed;
        this.level = level;
        this.exp = exp;
        this.money = money;
        this.name = name;
        this.towersImprovements = new TowersImprovements();
    }

    /**
     * Calculate crypted val and return it
     *
     * @return the crypt
     */
    public Crypt calculateCryptedVal(){
        if (towersImprovements == null){
            this.towersImprovements = new TowersImprovements();
        }
        // make encryption string
        StringBuilder test = new StringBuilder("HeroesTD"+levelMax+numberGamesPlayed+totalWavesNumber+exp+money+name);
        towersImprovements.getMapTkTowersImprovement().values().forEach(x-> test.append(x.getAttack()+""+x.getAttackRange()+""+x.getAttackRate()));

        // crypt string
        Crypt crypt = new Crypt(test.toString());
        crypt.encrypt();
        return crypt;
    }

    /**
     * Calculate uncrypted val crypt and return it
     *
     * @return the crypt
     */
    public Crypt calculateUncryptedVal(){
        if (towersImprovements == null){
            this.towersImprovements = new TowersImprovements();
        }
        // make encryption string
        StringBuilder test = new StringBuilder("HeroesTD"+levelMax+numberGamesPlayed+totalWavesNumber+exp+money+name);
        towersImprovements.getMapTkTowersImprovement().values().forEach(x-> test.append(x.getAttack()+""+x.getAttackRange()+""+x.getAttackRate()));
        // make test encryption string
        Crypt crypt = new Crypt(test.toString());
        return crypt;
    }

    /**
     * Load user return true if load was without issue
     *
     * @return true if loaded corrrectly else false
     */
    public boolean loadUser(){
        boolean bool;
        User userLoaded;
        Gson gson = new Gson();
        File file = new File("src/Heroes/data.json");
        // try to load from file and if everythings is great set value tho this user
        try {
            Reader reader = Files.newBufferedReader(file.toPath().toRealPath());
            userLoaded = gson.fromJson(reader, User.class);
            cryptedVal= userLoaded.calculateUncryptedVal();
            if (cryptedVal.equals(userLoaded.cryptedVal)){
                System.out.println("File is correct");
                //this = userLoaded;
                levelMax = userLoaded.levelMax;
                totalWavesNumber = userLoaded.totalWavesNumber;
                numberGamesPlayed = userLoaded.numberGamesPlayed;
                level = userLoaded.level;
                exp = userLoaded.exp;
                money = userLoaded.money;
                name = userLoaded.name;
                cryptedVal = calculateCryptedVal();
                towersImprovements = userLoaded.towersImprovements;
                bool = true;
            }
            else{
                // message if file data seems false
                bool = false;
            }
        }
        catch (IOException| NullPointerException e){
            // message if file error
            System.out.println("File is not filled correctly");
            bool = false;
        }
        cryptedVal = calculateCryptedVal();
        return bool;
    }

    /**
     * Read a new user
     *
     * @param s name of user
     */
    public void readNewUser(String s) {
        this.levelMax = 0;
        this.totalWavesNumber = 0;
        this.numberGamesPlayed = 0;
        this.level = 0;
        this.exp = 0;
        this.money = 0;
        this.name = s;
        this.towersImprovements = new TowersImprovements();
        this.cryptedVal = calculateCryptedVal();
    }


    /**
     * Save user to file
     */
    public void saveUser(){
        this.cryptedVal = calculateCryptedVal();
        Gson gson = new Gson();
        // try to save this to file
        File file = new File("src/Heroes/data.json");
        try {
            FileWriter write = new FileWriter(file);
            gson.toJson(this, write);
            write.close();
            System.out.println("Saved done !");
        }
        catch (IOException e){
            System.out.println(e);
        }
    }


    /**
     * Exp next level int.
     *
     * @return the int
     */
    public int expNextLevel(){
        return (this.level+1)^3*1000;
    }

    /**
     * Add exp.
     *
     * @param newExpWon the new exp won
     */
    public void addExp(int newExpWon){
        if (newExpWon+this.exp > expNextLevel()){
            newExpWon = newExpWon+this.exp-expNextLevel();
            this.level +=1;
            addExp(newExpWon);
        }
        else{
            this.exp = newExpWon;
        }
    }

    /**
     * Gets level max.
     *
     * @return the level max
     */
    public int getLevelMax() {
        return levelMax;
    }

    /**
     * Sets level max.
     *
     * @param levelMax the level max
     */
    public void setLevelMax(int levelMax) {
        this.levelMax = levelMax;
    }

    /**
     * Gets total waves number.
     *
     * @return the total waves number
     */
    public int getTotalWavesNumber() {
        return totalWavesNumber;
    }

    /**
     * Add total waves number.
     *
     * @param numberMonstersKilled the number monsters killed
     */
    public void addTotalWavesNumber(int numberMonstersKilled) {
        this.totalWavesNumber += numberMonstersKilled;
    }

    /**
     * Gets number games played.
     *
     * @return the number games played
     */
    public int getNumberGamesPlayed() {
        return numberGamesPlayed;
    }

    /**
     * Add number games played.
     *
     * @param numberGamesPlayed the number games played
     */
    public void addNumberGamesPlayed(int numberGamesPlayed) {
        this.numberGamesPlayed += numberGamesPlayed;
    }

    /**
     * Gets level.
     *
     * @return the level
     */
    public int getLevel() {
        return level;
    }

    /**
     * Gets exp.
     *
     * @return the exp
     */
    public int getExp() {
        return exp;
    }

    /**
     * Gets money.
     *
     * @return the money
     */
    public int getMoney() {
        return money;
    }

    /**
     * Add money.
     *
     * @param money the money
     */
    public void addMoney(int money) {
        this.money += money;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets towers improvements.
     *
     * @return the towers improvements
     */
    public TowersImprovements getTowersImprovements() {
        return towersImprovements;
    }

}